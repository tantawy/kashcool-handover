//
//  WebViewController.swift
//  Kashcool
//
//  Created by aya on 10/10/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import WebKit
class WebViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    

    var fileUrl : URL?
    override func viewDidLoad() {
        super.viewDidLoad()
       
            webView.scrollView.maximumZoomScale = 1
        webView.scrollView.minimumZoomScale = 1
            webView.scrollView.bouncesZoom = false
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
        webView.loadFileURL(fileUrl!, allowingReadAccessTo: fileUrl!.deletingLastPathComponent().deletingLastPathComponent().deletingLastPathComponent())
        webView.scrollView.bounces = false
        //print(fileUrl)
    }


    
    @IBAction func closeButtonTapped(_ sender: Any) {
        webView.stopLoading()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        self.dismiss(animated: true, completion: nil)
    }
    

}
