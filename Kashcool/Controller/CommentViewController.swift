//
//  CommentViewController.swift
//  Kashcool
//
//  Created by aya on 10/23/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire
import SwiftyJSON
import SVProgressHUD
class CommentViewController: UIViewController {

    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var cosmos: CosmosView!
    var rating = 0
    var subjectId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        if lang == "ar"{
            commentTextField.placeholder = "اضف تعليق ..."
        }else{
            commentTextField.placeholder = "Add comment"
        }
        cosmos.didFinishTouchingCosmos = { (_rating) in
            self.rating = Int(_rating)
        }

        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }

    

    @IBAction func postComment(_ sender: Any) {
        postComment()
        
        if rating > 0 {
            postRate()
        }
    }
    func postRate (){
        

        let parameters: [String: Any] = [
            "Data":
                [
                    "action": "AddRate",
                    "lang": lang
            ],
            "Request":[
                "api_key":api_Key,
                "user_id":userID,
                "subject_id":subjectId,
                "rate": rating
                
            ]
        ]
        
        Alamofire.request(URL(string: "https://kashcool.com.kw/api")!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if response.result.isSuccess {
                let json = JSON(response.data)
                print(json)
            }
        }
    }
    
    func postComment (){
        
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        let parameters: [String: Any] = [
            "Data":
                [
                    "action": "AddComment",
                    "lang": lang
            ],
            "Request":[
                "api_key":api_Key,
                "user_id":userID,
                "comment":commentTextField.text!,
                "operation_id":subjectId,
                "type":1
                
            ]
        ]
        Alamofire.request(URL(string: "https://kashcool.com.kw/api")!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if response.result.isSuccess {
                let json = JSON(response.data!)
                if json["Status"]["succeed"] == 1 {
                    SVProgressHUD.dismiss()
                    SVProgressHUD.setMaximumDismissTimeInterval(1)
                    SVProgressHUD.showSuccess(withStatus: "Done !")
                    self.dismiss(animated: true, completion: nil)
                }else{
                    SVProgressHUD.dismiss()
                    let msg = json["Status"]["message"][0].stringValue
                    let title = lang == "ar" ? "خطا" : "Error"
                    self.showErrorMessage(title: title, message: msg)
                }
            }else{
                SVProgressHUD.dismiss()
                let title = lang == "ar" ? "خطا" : "Error"
                let message = lang == "ar" ? "تاكد من الاتصال" : "Check your connection"
                self.showErrorMessage(title: title, message: message)
            }
        }
        
    }
    @IBAction func closePressed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func showErrorMessage(title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = lang == "ar" ? "حسنا" : "Ok"
        let action = UIAlertAction(title: ok, style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true)
    }
    
}
