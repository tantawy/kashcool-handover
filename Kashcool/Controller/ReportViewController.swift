//
//  ReportViewController.swift
//  Kashcool
//
//  Created by aya on 10/21/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire
class ReportViewController: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var fullName: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var phone: UITextField!
    
    @IBOutlet weak var _title: UITextField!
    
    @IBOutlet weak var content: UITextField!
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if lang == "en"{
            backBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            fullName.placeholder = "Full name"
            email.placeholder = "Email"
            phone.placeholder = "Phone number"
            _title.placeholder = "Title"
            content.placeholder = "Content"
            //content.contentHorizontalAlignment = .left
        }else{
            
            
          
            fullName.placeholder = "الاسم"
            email.placeholder = "الايميل"
            phone.placeholder = "رقم الهاتف"
            _title.placeholder = "العنوان"
            content.placeholder = "المحتوى"
        }
       
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }



    @IBAction func bcakBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
   
    @IBAction func sendReportPressed(_ sender: Any) {
        
        if fullName.text == "" || email.text == "" || phone.text == "" || _title.text == "" || content.text == ""{
            let title = lang == "ar" ? "حقل فارغ" : "Empty field"
            let message = lang == "ar" ? "من فضلك املأ جميع الحقول" : "Please fill all fields"
            showErrorMessage(title: title, message: message)
        }else{
            postProblem()
            
        }
        
    }
    
    
    func showErrorMessage(title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = lang == "ar" ? "حسنا" : "Ok"
        let action = UIAlertAction(title: ok, style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true)
    }
    
    func postProblem()   {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        let parameters: [String: Any] = [
            "Data":
                [
                    "action": "SENDREPORT",
                    "lang": lang
            ],
            "Request":[
                "name": fullName.text,
                "email": email.text,
                "phone" : phone.text,
                "title" : _title.text,
                "content" : content.text
                
            ]
        ]
        Alamofire.request(rootUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if response.result.isSuccess {
                print(response)
                let json = JSON(response.data!)
                if json["Status"]["succeed"].intValue == 1 {
                    SVProgressHUD.dismiss()
                    SVProgressHUD.setMaximumDismissTimeInterval(1)
                    SVProgressHUD.showSuccess(withStatus: "Report sent successfully")
                    self.dismiss(animated: true, completion: nil)
                }else {
                    SVProgressHUD.dismiss()
                    let title = lang == "ar" ? "خطا" : "Failure"
                    let message = lang == "ar" ? "حدث خطا ما" : "Something went wrong !"
                    self.showErrorMessage(title: title, message: message)
                }
            }else{
                SVProgressHUD.dismiss()
                let title = lang == "ar" ? "خطا" : "Failure"
                let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connection"
                self.showErrorMessage(title: title, message: message)
            }
        }
    }
}
