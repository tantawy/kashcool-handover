//
//  SettingsViewController.swift
//  Kashcool
//
//  Created by aya on 10/21/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import SideMenu
class SettingsViewController: UIViewController {

    
    static weak var instance : SettingsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SettingsViewController.instance == nil {
            SettingsViewController.instance = self
        }
        
    }


    @IBAction func englishBtnPressed(_ sender: Any) {

        Language.setAppLanguage(lang: "en")
        lang = "en"
       UISideMenuNavigationController.lang = "en"
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        
        NotificationCenter.default.post(name: .languageChanged, object: nil)
        
        HomeViewController.instance?.navigationController?.popToRootViewController(animated: false)
        

       HomeViewController.instance?.reload()
        
    }
    

    @IBAction func arabicBtnPressed(_ sender: Any) {

        Language.setAppLanguage(lang: "ar")
        lang = "ar"
        UISideMenuNavigationController.lang = "ar"
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        
       
        NotificationCenter.default.post(name: .languageChanged, object: nil)
        
        HomeViewController.instance?.navigationController?.popToRootViewController(animated: false)
        HomeViewController.instance?.reload()
        
    }
    
}
