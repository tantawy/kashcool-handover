//
//  UIViewExtention.swift
//  Kashcool
//
//  Created by aya on 9/26/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import ObjectiveC

private var controller : Any?



extension UIView {
    
    
    
     var resonsibleController : Any? {
        
        set (value) {
            objc_setAssociatedObject(self, &controller, value, .OBJC_ASSOCIATION_RETAIN)
        }
        get{
            return objc_getAssociatedObject(self, &controller) as Any
        }
    }
    
    
        @IBInspectable var borderColor : UIColor? {
            set (newValue) {
                self.layer.borderColor = (newValue ?? UIColor.clear).cgColor
            }
            get {
                return UIColor(cgColor: self.layer.borderColor!)
            }
            
        }
        
        @IBInspectable var borderWidth : CGFloat {
            set (newValue) {
                self.layer.borderWidth = newValue
            }
            get {
                return self.layer.borderWidth
            }
        }
        
        @IBInspectable var cornerRadius : CGFloat {
            set (newValue) {
                self.layer.cornerRadius = newValue
            }
            get {
                return self.layer.cornerRadius
            }
        }
        
    
    
}
