//
//  UIApplication.swift
//  Kashcool
//
//  Created by aya on 10/11/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
    
 
}


