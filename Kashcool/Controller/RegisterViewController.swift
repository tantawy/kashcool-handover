//
//  RegisterViewController.swift
//  Kashcool
//
//  Created by aya on 10/17/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
class RegisterViewController: UIViewController , UIPickerViewDelegate , UIPickerViewDataSource {

    

    @IBOutlet weak var btn1: UIButton!
    
    @IBOutlet weak var btn2: UIButton!
    
    @IBOutlet weak var btn3: UIButton!
    
    
    @IBOutlet weak var classBtn: UIButton!
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    var type = 1
    var selectedColor : UIColor?
    var classId = 2
    var dataSource = ["Grade k-1 (elementary)","Grade k-2 (elementary)","Grade k-3 (elementary)","Grade k-4 (elementary)","Grade k-5 (elementary)","Grade k-6 (intermediate)","Grade k-7 (intermediate)","Grade k-8 (intermediate)","Grade k-9 (intermediate)","Grade k-10 (secondary)","Grade k-11 (secondary, literary division)","Grade k-12 (secondary, literary division)","Grade k-11 (secondary, science division)","Grade k-12 (secondary, science division)"]

    var classIDs = [2,3,4,5,6,8,9,10,11,14,16,17,18,19]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedColor = btn1.backgroundColor
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        if lang == "ar"{
            userNameTextField.placeholder = "رقم الهاتف"
            passwordTextField.placeholder = "كلمه المرور"
            confirmPasswordTextField.placeholder = "تاكيد كلمه المرور"
            btn1.setTitle("ولى امر", for: .normal)
            btn2.setTitle("طالب", for: .normal)
            btn3.setTitle("معلم", for: .normal)
        }
        else{
            userNameTextField.placeholder = "Phone number"
            passwordTextField.placeholder = "Password"
            confirmPasswordTextField.placeholder = "Confirm pasword"
            btn1.setTitle("Parent", for: .normal)
            btn2.setTitle("Student", for: .normal)
            btn3.setTitle("Teacher", for: .normal)
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
        @objc func dismissKeyboard() {
            
            view.endEditing(true)
        }
    
    
    @IBAction func selectionBtnPressed(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            type = 1
            btn1.backgroundColor = selectedColor
            btn1.setTitleColor(.white, for: .normal)
            btn2.backgroundColor = .white
            btn2.setTitleColor(.lightGray, for: .normal)
            btn3.backgroundColor = .white
            btn3.setTitleColor(.lightGray, for: .normal)
            classBtn.isHidden = true
        case 1:
            type = 2
            btn2.backgroundColor = selectedColor
            btn2.setTitleColor(.white, for: .normal)
            btn3.backgroundColor = .white
            btn3.setTitleColor(.lightGray, for: .normal)
            btn1.backgroundColor = .white
            btn1.setTitleColor(.lightGray, for: .normal)
            classBtn.isHidden = false
        case 2:
            type = 3
            btn3.backgroundColor = selectedColor
            btn3.setTitleColor(.white, for: .normal)
            btn2.backgroundColor = .white
            btn2.setTitleColor(.lightGray, for: .normal)
            btn1.backgroundColor = .white
            btn1.setTitleColor(.lightGray, for: .normal)
            classBtn.isHidden = true
        default: break
            
        }
        
    }
    
    
    
    
    
    func showErrorMessage(title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = lang == "ar" ? "حسنا" : "Ok"
        let action = UIAlertAction(title: ok, style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true)
    }
    
    @IBAction func register(_ sender: UIButton) {
        
        if userNameTextField.text == "" {
            
            let message = lang == "ar" ? "من فضلك ادخل رقم الهاتف" : "Please enter phone number"
            showErrorMessage(title: "", message: message)
        }
        else if passwordTextField.text == "" {
            let title = lang == "ar" ? "خطا" : "Password required"
            let message = lang == "ar" ? "من فضلك ادخل كلمه المرور" : "Please enter your password"
            showErrorMessage(title: title, message: message)
        }
        else if confirmPasswordTextField.text != passwordTextField.text {
            let title = lang == "ar" ? "خطا" : "Password not match"
            let message = lang == "ar" ? "تاكد من صحه كلمه المرور" : "Please match your password"
            showErrorMessage(title: title, message: message)
        }
        else{
            doregister()
        }
        
    }
    func doregister() {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        var parameters: [String: Any]
        if type == 2 {
        parameters = [
            "mobile" : userNameTextField.text!
            ,"password" : passwordTextField.text!
            ,"password_confirmation" : passwordTextField.text!
            ,"platform" : "ios"
            ,"type" : type
            ,"class_id" : classId
            
            ]
            
        }else{
            parameters = [
                "mobile" : userNameTextField.text!
                ,"password" : passwordTextField.text!
                ,"password_confirmation" : passwordTextField.text!
                ,"platform" : "ios"
                ,"type" : type

                
            ]
        }
        Alamofire.request(rootUrl + "/v2/user/register", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if response.result.isSuccess{
                print(response)
                let json = JSON(response.data!)
                if json["status"]["succeed"].intValue == 1 {
                    print(json)
                    SVProgressHUD.dismiss()
                    SVProgressHUD.setMaximumDismissTimeInterval(1)
                    SVProgressHUD.showSuccess(withStatus: "Login successful")
                    self.setUserData(json: json)
                }else{
                    SVProgressHUD.dismiss()
                    let title = lang == "ar" ? "خطا فى البيانات" : "Invalid data"
                    let message = lang == "ar" ? "تاكد من البيانات المدخله" : "You have entered wrong data please try again"
                    self.showErrorMessage(title: title, message: message)
                    
                }
                
            }else {
                SVProgressHUD.dismiss()
                let title = lang == "ar" ? "خطا فى الاتصال" : "Connection Error"
                let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connction"
                self.showErrorMessage(title: title, message: message)
                print("LoginError")
            }
        }
        
        
        
    }
    weak var  homeVC : UIViewController?
    func setUserData(json : JSON) {
        fullName = json["user"]["full_name"].stringValue
        userID = json["user"]["id"].intValue
        email = json["user"]["email"].stringValue
        api_Key = json["user"]["api_key"].stringValue
        avatarUrl = json["user"]["avatar_url"].stringValue
        classID = json["user"]["class_id"].intValue
        mobile = json["user"]["mobile"].stringValue
        // saving data to realm database for offline mode
        let userData = UserData()
        
        
        userData.fullName = fullName
        userData.userID = userID
        userData.email = email
        userData.api_Key = api_Key
        userData.avatarUrl = avatarUrl
        userData.classID = classID
        userData.mobile = mobile
        
        
        userData.store()
        
        homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeRoot")
        present(homeVC!, animated: true, completion: nil)
        homeVC = nil
    }

    @IBAction func classBtnTapped(_ sender: Any) {
        
        pickerView.isHidden = false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        classId = classIDs[row]
        classBtn.setTitle(dataSource[row], for: .normal)
        pickerView.isHidden = true
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource[row]
    }
    
    @IBAction func goToSignIn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
