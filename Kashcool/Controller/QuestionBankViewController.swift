//
//  QuestionBankViewController.swift
//  Kashcool
//
//  Created by aya on 10/22/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown
import SVProgressHUD
class QuestionBankViewController: UIViewController , UITableViewDelegate ,UITableViewDataSource ,UICollectionViewDelegate,UICollectionViewDataSource {

    
    var score = 0
    var subjectId = 0
    var questions = [Question]()
    var subjectTitle = ""
    var className = ""
    var termName = ""
    var year = ""
    var subjectLanguage = ""
    var numberOfQuestions = 0
    var testTime = 0
    var typesOfQuestions = ""
    var fullExamTime = 0
    
    
    var mySubjects = [MySubject]()
    @IBOutlet weak var back: UIButton!
    
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var subjectsView: UIView!
    
    @IBOutlet weak var blockView: UIImageView!
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var subjectsCollectionview: UICollectionView!
    @IBOutlet weak var examView: UIView!
    @IBOutlet weak var examDetailsView: UIView!
    
    
    @IBOutlet weak var finishedView: UIView!
    @IBOutlet weak var subjectTitleLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    
    @IBOutlet weak var termLabel: UILabel!
    
    @IBOutlet weak var yearLabel: UILabel!
    
    @IBOutlet weak var QuestionNumbersLabel: UILabel!
    
    @IBOutlet weak var examTimeLabel: UILabel!
    
    @IBOutlet weak var questionTypesLabel: UILabel!
    
    /////////////////////////////////////////////
    @IBOutlet weak var questionTypeLabel: UILabel!
    
    @IBOutlet weak var endBtn: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    
    
    @IBOutlet weak var body: UILabel!
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var nextBtn: UIButton!
    
    
    @IBOutlet weak var trueFalseView: UIView!

    @IBOutlet weak var choicesView: UIView!
    
    @IBOutlet weak var choicesTable: UITableView!
    
    
    @IBOutlet weak var dropDownView: UIView!
    
    @IBOutlet weak var dropDownAnswerLabel: UILabel!
    @IBOutlet weak var dropDownContainer: UIView!
    @IBOutlet weak var trueBtn: UIButton!
    @IBOutlet weak var falseBtn: UIButton!
    let dropDown = DropDown()
    
    var currentQuestionIdex = 0
    
    var countdownTimer: Timer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if lang == "en"{
            back.transform = CGAffineTransform(scaleX: -1, y: -1)
        }
        choicesTable.delegate = self
        choicesTable.dataSource = self
        choicesTable.setEditing(true, animated: false)
        
        dropDown.anchorView = dropDownContainer
        dropDown.direction = .bottom
        dropDown.selectionAction = { [unowned self] (_, answer: String) in
            self.dropDownSelectAnswer(answer: answer)
        }
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dropDownTapped))
        dropDownContainer.addGestureRecognizer(gesture)
        subjectsCollectionview.delegate = self
        subjectsCollectionview.dataSource = self
//        requestMySubjects()
       // getData()
    }

    
    @objc func getData(_sender : UIButton) {
        
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        blockView.isHidden = false
        let sender = _sender.resonsibleController as! SubjectQBCellCollectionViewCell
        let id = sender.subjectId
        
        examDetailsView.isHidden = false
        Alamofire.request(URL(string: "https://kashcool.com.kw/api/v2/question-bank/\(id)" )!,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["Authorization": "Bearer " + api_Key,"Content-Language" :lang]).responseJSON {[weak self] (response) in
            
            if response.result.isSuccess {
                let json = JSON(response.data!)
                
                if json["status"]["succeed"].intValue == 1 {
                    print(response)
                    self?.blockView.isHidden = true
                    self?.parseSubjectDetails(json: json["result"])
                    self?.parseQuestions(json: json["result"]["questions"])
                    SVProgressHUD.dismiss()
                    print(response)
                }else{
                    
                    SVProgressHUD.dismiss()
                    self?.noDataView.isHidden = false
                }
            }else{
                self?.blockView.isHidden = true
                self?.noDataView.isHidden = false
                SVProgressHUD.dismiss()
                print(response)
                print("can't get data")
            }
        }
        
        
    }
    
    func parseSubjectDetails(json : JSON){
        subjectId = json["subject_id"].intValue
        subjectTitle = json["title"].stringValue
        className = json["class_name"].stringValue
        termName = json["term_name"].stringValue
        year = json["year"].stringValue
        subjectLanguage = json["subject_lang"].stringValue
        numberOfQuestions = json["number_of_questions"].intValue
        if numberOfQuestions == 0 {
            noDataView.isHidden = false
            return
        }
        testTime = json["test_time_per_minute"].intValue * 60
        typesOfQuestions = ""
        for index in 0...json["types_of_questions"].count - 1 {
            typesOfQuestions = typesOfQuestions + json["types_of_questions"][index].stringValue
            if index != json["types_of_questions"].count - 1{
                
                typesOfQuestions = typesOfQuestions + ","
            }
        }
        subjectTitleLabel.text = subjectTitle
        classLabel.text = className
        termLabel.text = termName
        yearLabel.text = year
        QuestionNumbersLabel.text = "\(numberOfQuestions)"
        examTimeLabel.text = "\(testTime/60)"
        timerLabel.text = "\(timeFormatted(testTime))"
        questionTypesLabel.text = typesOfQuestions
        print(typesOfQuestions)
    }
    
    func parseQuestions (json : JSON){
         print("questions")
        
        let trueFalseArray = json["trueFalse"].arrayValue
        if trueFalseArray.count > 0{
            parseTrueFalse(json: trueFalseArray)
        }
        
        let coicesArray = json["choices"].arrayValue
        if coicesArray.count > 0{
            parseChoices(json: coicesArray)
        }

        let fillBlankDropDownArray = json["fillBlankDropDown"].arrayValue
        if fillBlankDropDownArray.count > 0{
            parseFillBlankDropDown(json: fillBlankDropDownArray)
        }
        
        
    }
    
    func startExam(){
        fullExamTime = testTime
        presentQuestion()
        startTimer()
    }
    
    func parseTrueFalse (json : [JSON]){
        print("trueFalse")
        for questionJson in json {
            let question = Question()
            question.type = "trueFalse"
            question.body = questionJson["body"].stringValue
            question.coAnswer = questionJson["co_answer"].stringValue
            question.answers = questionJson["answers"].arrayObject as! [String]
            
            questions.append(question)
        }
        
    }
    func parseChoices (json : [JSON]){
        print("coices")
        for questionJson in json {
            let question = Question()
            question.type = "choices"
            question.body = questionJson["body"].stringValue
            question.coAnswer = questionJson["co_answer"][0].stringValue
            question.answers = questionJson["answers"].arrayObject as! [String]
            questions.append(question)
        }
    }
    
    func parseFillBlankDropDown (json : [JSON]){
        print("fillBlankDropDown")
        for questionJson in json {
            let question = Question()
            question.type = "fillBlankDropDown"
            question.body = questionJson["body"].stringValue
            question.coAnswer = questionJson["co_answer"][0].stringValue
            question.answers = questionJson["answers"][0].arrayObject as! [String]
            questions.append(question)
        }
    }
    
    
    func presentQuestion (){
        if currentQuestionIdex == 0 {
            backBtn.alpha = 0
            backBtn.isEnabled = false
        }else{
            backBtn.alpha = 1
            backBtn.isEnabled = true
        }
        if currentQuestionIdex == questions.count - 1 {
            nextBtn.isEnabled = false
            nextBtn.alpha = 0
        }
        else{
            nextBtn.isEnabled = true
            nextBtn.alpha = 1
        }
        let question = questions[currentQuestionIdex]
        /////////////////////////////////////////////////
        if question.type == "trueFalse"{
            questionTypeLabel.text = "True Or False questions"
            if lang == "ar"{
                questionTypeLabel.text = "صح و خطأ"
            }
            trueFalseView.isHidden = false
            dropDownView.isHidden = true
            choicesView.isHidden = true
            body.text = question.body
            if question.answer != ""{
                
                if question.answer == "true"{
                    trueBtn.backgroundColor = .green
                    falseBtn.backgroundColor = UIColor(rgb: 0xE6E6E6)
                }else{
                    trueBtn.backgroundColor = UIColor(rgb: 0xE6E6E6)
                    falseBtn.backgroundColor = .green
                }
            }else{
                trueBtn.backgroundColor = UIColor(rgb: 0xE6E6E6)
                falseBtn.backgroundColor = UIColor(rgb: 0xE6E6E6)
            }
        }
        ////////////////////////////////////////////////
        if question.type == "choices"{
            questionTypeLabel.text = "Choices answers"
            if lang == "ar"{
                questionTypeLabel.text = "اختياري"
            }
            trueFalseView.isHidden = true
            dropDownView.isHidden = true
            choicesView.isHidden = false
            
            body.text = question.body
            choicesTable.reloadData()
            
            
            if questions[currentQuestionIdex].answerNo > 0 {
                
                choicesTable.selectRow(at: IndexPath(row: questions[currentQuestionIdex].answerNo - 1, section: 0), animated: false, scrollPosition: UITableView.ScrollPosition(rawValue: 0)!)
                
            }
        }
        
        ////////////////////////////////////////
        if question.type == "fillBlankDropDown"{
            questionTypeLabel.text = "Fill questions"
            if lang == "ar"{
                questionTypeLabel.text = "اكمل"
            }
            trueFalseView.isHidden = true
            dropDownView.isHidden = false
            choicesView.isHidden = true
            
            body.text = question.body
            dropDown.dataSource = question.answers
            
            if questions[currentQuestionIdex].answer != "" {
                
                dropDownAnswerLabel.text = questions[currentQuestionIdex].answer
                
            }else{
                dropDownAnswerLabel.text = "Select correct answer"
            }
            
        }
        
    }
    
    
    
    func answerQuestion (answer : String){
        
        questions[currentQuestionIdex].answer = answer
    }
    
    @IBAction func answerTrue(_ sender: Any) {
        trueBtn.backgroundColor = .green
        falseBtn.backgroundColor = UIColor(rgb: 0xE6E6E6)
        answerQuestion(answer: "true")
    }
    
    @IBAction func answerFalse(_ sender: Any) {
        trueBtn.backgroundColor = UIColor(rgb: 0xE6E6E6)
        falseBtn.backgroundColor = .green
        answerQuestion(answer: "false")
    }
    
    @IBAction func Back(_ sender: Any) {
        currentQuestionIdex = currentQuestionIdex - 1
        presentQuestion()
    }
    
    

    @IBAction func Next(_ sender: Any) {
        currentQuestionIdex = currentQuestionIdex + 1

        presentQuestion()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if questions.count > 0{
            return questions[currentQuestionIdex].answers.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = choicesTable.dequeueReusableCell(withIdentifier: "ChoiceCell") as! ChoiceCell
        
        cell.answerLabel.text = questions[currentQuestionIdex].answers[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle(rawValue: 3)!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let answers = questions[currentQuestionIdex].answers
        questions[currentQuestionIdex].answer = answers[indexPath.item]
        questions[currentQuestionIdex].answerNo = indexPath.item + 1
        
       
        for index in 0...answers.count {
            
            if indexPath.item != index{
                
                choicesTable.deselectRow(at: IndexPath(row: index, section: 0), animated: false)
            }
        }
        
    }
    
   @objc func dropDownTapped (){
    dropDown.show()
    }
    
    
    func dropDownSelectAnswer (answer : String){
        dropDownAnswerLabel.text = answer
        questions[currentQuestionIdex].answer = answer
        
    }
    
    ////////////////////////////////////
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        timerLabel.text = "\(timeFormatted(testTime))"
        
        if testTime != 0 {
            testTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
        endExam()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    @IBAction func startExamPressed(_ sender: Any) {
        NotificationCenter.default.post(name: .languageChanged, object: nil)
        examView.isHidden = false
        examDetailsView.isHidden = true
        startExam()
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func endBtnPressed(_ sender: Any) {
        print("XXXX")
        endExam()
    }
    
    
    func endExam(){
        for question in questions {
            
            if question.answer == question.coAnswer{
                score = score + 1
            }
        }
        finishedView.isHidden = false
    }
    
    
    func sendExamReport(){
        
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let currnetTime = formatter.string(from: date)
        
        let url = rootUrl + "/v2/question-bank/\(subjectId)/exam"
        let parameters: [String: Any] = [
            "num_questions": numberOfQuestions,
            "answer_duration": (fullExamTime - testTime)/60,
            "num_correct_answers":score,
            "num_wrong_answers":numberOfQuestions - score,
            "exam_started_at": currnetTime,
            "mark":Float(score)/Float(questions.count) * 100
            
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ["Authorization": "Bearer " + api_Key,"Content-Language":lang]).responseJSON { (response) in
            print(response)
        }
        
    }


    
    @IBAction func sendExamReport(_ sender: Any) {
        
        sendExamReport()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if mySubjects.count == 0{
            return 0}else{
            return mySubjects.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = subjectsCollectionview.dequeueReusableCell(withReuseIdentifier: "SubjectCell", for: indexPath) as! SubjectQBCellCollectionViewCell
        cell._class.text = mySubjects[indexPath.item].classTitleEn
        cell.subjectId = mySubjects[indexPath.item].id
        cell.subjectImage.sd_setImage(with: URL(string: mySubjects[indexPath.item].imageUrl), completed: nil)
        cell.subjectTitle.text = mySubjects[indexPath.item].title
        cell.term.text = mySubjects[indexPath.item].termFullName
        cell.startExamBtn.resonsibleController = cell
        cell.startExamBtn.removeTarget(nil, action: nil, for: .allEvents)
        cell.startExamBtn.addTarget(self, action: #selector(getData(_sender:)), for: .touchUpInside)
        return cell
    }
    
    func requestMySubjects(){
        
        
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        blockView.isHidden = false
        let parameters: [String: Any] = [
            "Data":
                [
                    "action": "MYSUBJECTS",
                    "lang": lang
            ],
            "Request":[
                "api_key":"BQETjbJU69vTs80vF48VNWXEF3I9nrvi",
                "user_id":userID
                
            ]
        ]
        Alamofire.request(URL(string: rootUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ["Authorization": "Bearer " + api_Key]).responseJSON {[unowned self] (response) in
            if response.result.isSuccess {
                let jsonData = JSON(response.data!)
                if jsonData["Status"]["succeed"].intValue == 1 {
                    self.createMySubjectsArray(json: jsonData["Result"]["Subjects"].array!)
                    SVProgressHUD.dismiss()
                    self.blockView.isHidden = true
                    
                }else{
                    SVProgressHUD.dismiss()
                    self.blockView.isHidden = true
                    self.errorView.isHidden = false
                    print("failed")
                }
            }else{
                SVProgressHUD.dismiss()
                self.blockView.isHidden = true
                self.errorView.isHidden = false
                print("failed")
            }
        }
    }
    
    
    func createMySubjectsArray (json : [JSON]){
        var mySubject : MySubject
        for index in 0...json.count-1{
            mySubject = MySubject()
            mySubject.id = json[index]["id"].intValue
            mySubject.title = json[index]["title"].stringValue
            mySubject.termFullName = json[index]["term_full_name"].stringValue
            mySubject.imageUrl = json[index]["image_url"].stringValue
            mySubject.classTitleAr = json[index]["get_class"]["title_ar"].stringValue
            mySubject.classTitleEn = json[index]["get_class"]["title_en"].stringValue
            
            mySubjects.append(mySubject)
        }
        subjectsCollectionview.reloadData()
        subjectsView.isHidden = false
        //instantiateSubjectCustomViews()
    }
    
    
    @IBAction func findExams(_ sender: Any) {
        requestMySubjects()
    }
    
    @IBAction func presentReports(_ sender: Any) {
    }
    
    @IBAction func subViewback(_ sender: UIButton) {
        
        noDataView.isHidden = true
        errorView.isHidden = true
        finishedView.isHidden = true
        examView.isHidden = true
        examDetailsView.isHidden = true
    }
    
}
