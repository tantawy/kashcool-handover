//
//  HelpWebViewController.swift
//  Kashcool
//
//  Created by aya on 10/21/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import WebKit
class HelpWebViewController: UIViewController {

    
    @IBOutlet weak var backBtn: UIButton!
    var url : String?
    
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if lang == "en" {
            backBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        let request = URLRequest(url: URL(string: url!)!)

        webView.load(request)
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
