//
//  Language.swift
//  Kashcool
//
//  Created by aya on 10/21/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
class Language {
    
    class func currentLanguage () -> String {
        let def = UserDefaults.standard
        let langs = def.object(forKey: "AppleLanguages") as! NSArray
        let firstLang = langs.firstObject as! String
        print(firstLang)
        return firstLang
    }
    
    class func setAppLanguage (lang : String) {
        let def = UserDefaults.standard
        def.set([lang], forKey: "AppleLanguages")
        def.synchronize()
    }
}
