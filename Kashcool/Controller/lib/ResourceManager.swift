//
//  ResourceManager.swift
//  Kashcool
//
//  Created by aya on 10/4/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import Alamofire
import Zip
import SSZipArchive
class ResourceManager  {

    func createDirectory(directory : String){
        // create path
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        let documentUrl = URL(string: documentPath!)
        // append directory
        let directoryUrl = documentUrl?.appendingPathComponent(directory)
        print(directoryUrl!.path)
        
        // check if exists or not
        if !FileManager.default.fileExists(atPath: directoryUrl!.path){
            // create directory
            do{
                try FileManager.default.createDirectory(atPath: directoryUrl!.path, withIntermediateDirectories: true, attributes: nil)
            }
            catch{
                print("can't create directory \(error.localizedDescription)")
            }
            
        }
    }
    
    func fileExist (fileName : String? , path : String) -> Bool{
        
        
        // create path
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        let documentUrl = URL(string: documentPath!)
        // append directory
        let directoryUrl = documentUrl?.appendingPathComponent(path)
        let fileUrl = fileName == nil ? directoryUrl : directoryUrl?.appendingPathComponent(fileName!)
        if FileManager.default.fileExists(atPath: fileUrl!.path){
            return true
        }else{
            return false
        }
    }
    
    
    func downloadFile (url : String , in folder : String , name : String , completion : (( _ destinationURL : URL?) -> Void)? , _progress : ((_ progress : Double)-> Void)?){
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let folderUrl = documentsURL.appendingPathComponent(folder)
        let fullUrl = folderUrl.appendingPathComponent(name)
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            
            return (fullUrl, [.removePreviousFile])
        }
        
        
        Alamofire.download(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["Authorization": "Bearer " + api_Key , "lang" : lang], to: destination).downloadProgress { (progress) in
            print(progress.fractionCompleted)
            
            _progress?(progress.fractionCompleted)
            
            }.response { (response) in
                if response.error == nil{
                completion?(response.destinationURL!)
                print(response.destinationURL!)
                }else{
                    
                    completion?(nil)
                }
        }
    }
    
    func unzipFile (url : URL? , destination : URL , password : String? , completion :((_ extractedFile : String) -> Void)?){
        
        if url == nil {
            return
        }
        
        var extractedFile :String?
        SSZipArchive.unzipFile(atPath: url!.path, toDestination: destination.path, overwrite: true, password: password, progressHandler: {(str , _ , _ , _)in
                    extractedFile = str
                    
                 }, completionHandler: { (str, success, err) in
                    if success {
                        if extractedFile != nil{
                        completion?(extractedFile!)
                        }
                        
                    }
        
                    if let error = err {
                        print(error)
                    }
                })

    }
 
    
    func removeFile (url : URL){
        let fileMangaer = FileManager.default
        do {
            try fileMangaer.removeItem(at: url)
        }catch{
            
        }
    }
    

    
}
