//
//  HomeViewController.swift
//  Kashcool
//
//  Created by aya on 9/24/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenu
import RealmSwift
import SVProgressHUD
let rootUrl = "https://kashcool.com.kw/api"
var fullName = "kashcool team"
var email = "user_22020070@email.com"
var mobile = "22020070"
var api_Key = "BQETjbJU69vTs80vF48VNWXEF3I9nrvi"
var aboutUs = "https://kashcool.com.kw/en/mobile/aboutus"
var userFolderName = "g6"
var classID = 8
var userID = 848
var userType = "Student"
var avatarUrl = "https://kashcool.com.kw/uploadedimg/user/avatar.png"
var lang = "en"



class HomeViewController: UIViewController , UIScrollViewDelegate {

    
    // Outlets
    @IBOutlet weak var schoolCoursesScrollView: UIScrollView!
    @IBOutlet weak var schoolCoursesStackView: UIStackView!
    
    @IBOutlet weak var mySubjectsScrollView: UIScrollView!
    
    @IBOutlet weak var mySubjectsStackView: UIStackView!
    
    @IBOutlet weak var demoSubjectScrollView: UIScrollView!
    @IBOutlet weak var demoSubjectStackView: UIStackView!
    
    var classes = [_class]()
    var mySubjects = [MySubject]()
    var lastButtonPressedTag = 0
    var lasButtonPressedTag = 0
   
    @IBOutlet weak var activityInicator1: UIActivityIndicatorView!
    
    @IBOutlet weak var activityIndicator2: UIActivityIndicatorView!
    
    @IBOutlet weak var activityIndicator3: UIActivityIndicatorView!
    
    @IBOutlet weak var serverError1: UIImageView!
    
    @IBOutlet weak var serverError2: UIImageView!
    
    @IBOutlet weak var serverError3: UIImageView!
    let refreshControll1 = UIRefreshControl()
    let refreshControll2 = UIRefreshControl()
    let refreshControll3 = UIRefreshControl()
    @IBOutlet weak var contentsScrollView: UIScrollView!
    
    
    @IBOutlet weak var selectionBar: UIView!
    
    static weak var instance : HomeViewController?
    static var navController : UINavigationController?
    
//    override func viewWillDisappear(_ animated: Bool) {
//        SVProgressHUD.dismiss()
//    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
        selectionBar.frame.offsetBy(dx: 0, dy: 0)
        if HomeViewController.instance == nil {
            print("rrrrr")
            HomeViewController.instance = self
            self.navigationController?.navigationBar.isHidden = true
            
        }
       menuController.currentController = self
       contentsScrollView.delegate = self
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        let greenColor = UIColor(red: 40, green: 152, blue: 146)
        SideMenuManager.default.menuAnimationBackgroundColor = greenColor
        UIApplication.shared.statusBarView?.backgroundColor = greenColor
       // requestAllClasses()
       // requestMySubjects()
        
        
        schoolCoursesScrollView.addSubview(refreshControll1)
        mySubjectsScrollView.addSubview(refreshControll2)
        demoSubjectScrollView.addSubview(refreshControll3)
        refreshControll1.tintColor = .green
        refreshControll2.tintColor = .green
        refreshControll3.tintColor = .green
        
        refreshControll1.addTarget(self, action: #selector(reloadClasses), for: UIControl.Event.valueChanged)
        refreshControll2.addTarget(self, action: #selector(reloadMysubjects), for: UIControl.Event.valueChanged)
        refreshControll3.addTarget(self, action: #selector(reloadClasses), for: UIControl.Event.valueChanged)
       
        // this is just for waiting for view to end animation to set content scroll view to the right position and requesting data from api
        DispatchQueue.main.asyncAfter(deadline: (.now())) {
            self.reload()
        }
    }

    @objc func reloadClasses (){
//        for view in demoSubjectStackView.arrangedSubviews{
//            demoSubjectStackView.removeArrangedSubview(view)
//            view.removeFromSuperview()
//        }
//        for view in schoolCoursesStackView.arrangedSubviews{
//            schoolCoursesStackView.removeArrangedSubview(view)
//            view.removeFromSuperview()
//        }
        classes.removeAll()
        requestAllClasses()
    }
    
    @objc func reloadMysubjects (){
        

        mySubjects.removeAll()
        requestMySubjects()
    }
    func reload (){
        
        
        emptyClasses()
        emptySubjects()
        reloadClasses()
        reloadMysubjects()
        
        resetContentScrollView()
        
        if lang == "ar" {
            print("YYYYYYYYr")
            print(contentsScrollView.frame.size.width)
            contentsScrollView.setContentOffset(CGPoint(x: contentsScrollView.frame.size.width * CGFloat(2), y: 0.0), animated: false)
        }
        
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        let page = getCurrentPage()
       
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animateAlongsideTransition(in: nil, animation: nil) { (_) in
            self.contentsScrollView.setContentOffset(CGPoint(x: self.contentsScrollView.frame.size.width * CGFloat(page), y: 0.0), animated: true)

        }
        
   
    }
    func showItSelf (){
       // print("showing")
        present(self, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
      //  currentViewContoller = self
       // homeViewController = self
    }

    override var prefersStatusBarHidden: Bool {
        return false
       
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func setNeedsStatusBarAppearanceUpdate() {
       // UIApplication.shared.isStatusBarHidden = false
        
    }


    @IBAction func segmentButtonTapped(_ sender: UIButton) {
        var tag = sender.tag
        if lang == "ar" {
            if tag == 0 {
                tag = 2
                print(tag)
            }
            else if tag == 2 {
                tag = 0
                print(tag)
            }
        }
        contentsScrollView.setContentOffset(CGPoint(x: contentsScrollView.frame.size.width * CGFloat(tag), y: 0.0), animated: true)
        lastButtonPressedTag = tag
    }
    
    func resetContentScrollView(){
        
        print("ZZZZZZZ")
        contentsScrollView.setContentOffset(CGPoint(x: contentsScrollView.frame.size.width * CGFloat(0), y: 0.0), animated: true)
        lastButtonPressedTag = 0
    }
    // MARK:- all classes section
    //////////////////////////////////////////////////////////////
    
    func requestAllClasses(){
        activityInicator1.isHidden = false
        activityIndicator3.isHidden = false
        serverError1.isHidden = true
        serverError3.isHidden = true
        let parameters: [String: Any] = [
            "Data":
                [
                    "action" : "GETCLASSES",
                    "lang" : lang
            ]
        ]
        print("requesting classes")
        Alamofire.request(URL(string: rootUrl)!,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ["Authorization": "Bearer " + api_Key]).responseJSON {[weak self] (response) in
            
            if response.result.isSuccess {
                let jsonData = JSON(response.data!)
                //print(jsonData)
                if jsonData["Status"]["succeed"].intValue == 1 {
                    self?.emptyClasses()
                    self?.createClassesArray(json: jsonData["Result"]["classes"].array!)
                    self?.activityInicator1.isHidden = true
                    self?.activityIndicator3.isHidden = true
                    self?.refreshControll1.endRefreshing()
                    self?.refreshControll3.endRefreshing()
                    
                }else{
                    
                    // trying to retrive classes from realm data base
                    if self?.realmClasses() != nil {
                        
                        self?.emptyClasses()
                        self?.instantiateClassCustomViews(classes: (self?.realmClasses())!)
                        self?.instantiateDemoClassCustomView(classes: (self?.realmClasses())!)
                        self?.activityInicator1.isHidden = true
                        self?.activityIndicator3.isHidden = true
                        self?.refreshControll1.endRefreshing()
                        self?.refreshControll3.endRefreshing()
                        
                    }else{
                    print("failed")
                    self?.activityInicator1.isHidden = true
                    self?.activityIndicator3.isHidden = true
                    self?.refreshControll1.endRefreshing()
                    self?.refreshControll3.endRefreshing()
                    self?.emptyClasses()
                    }
                }
            }else{
                
                if self?.realmClasses() != nil {
                    
                    self?.emptyClasses()
                    self?.instantiateClassCustomViews(classes: (self?.realmClasses())!)
                    self?.instantiateDemoClassCustomView(classes: (self?.realmClasses())!)
                    self?.activityInicator1.isHidden = true
                    self?.activityIndicator3.isHidden = true
                    self?.refreshControll1.endRefreshing()
                    self?.refreshControll3.endRefreshing()
                    
                }else{
                print("can't get data")
                self?.serverError1.isHidden = false
                self?.serverError3.isHidden = false
                self?.activityInicator1.isHidden = true
                self?.activityIndicator3.isHidden = true
                self?.refreshControll1.endRefreshing()
                self?.refreshControll3.endRefreshing()
                self?.emptyClasses()
                }
            }
        }
    }
    
    
    func emptyClasses () {
        
        for view in demoSubjectStackView.arrangedSubviews{
            demoSubjectStackView.removeArrangedSubview(view)
            view.removeFromSuperview()
            
        }
        for view in schoolCoursesStackView.arrangedSubviews{
            schoolCoursesStackView.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
    
    
    func createClassesArray(json : [JSON]){
       
        

        for index in 0...json.count-1 {
            
            let myclass = _class()
            myclass.id = json[index]["id"].intValue
            //print(myclass.id )
            myclass.title = json[index]["title"].stringValue
            myclass.imageUrl = json[index]["image_url"].stringValue
            classes.append(myclass)
            myclass.store()
        }
        //storeInRealm()
        //print(classes)
        instantiateClassCustomViews(classes: classes)
        instantiateDemoClassCustomView(classes: classes)
    }
    

    
    
    func realmClasses () -> [_class]?{
        
        let results = try! Realm().objects(_class.self)
        let array = Array(results).filter({$0.language == lang})
        return array.count > 0 ? array : nil
        
    }
    
    func realmMySubjects () -> [MySubject]? {
        let results = try! Realm().objects(MySubject.self)
        let array = Array(results).filter({$0._userID == userID && $0.language == lang})
        return array.count > 0 ? array : nil
    }
    
    static func realmDemoSubjects (for_class classID : Int) -> [DemoSubject]?{
        
        let results = try! Realm().objects(DemoSubject.self)
        let array = Array(results).filter({$0._classID == classID && $0.language == lang})
        return array.count > 0 ? array : nil
        
    }
    
    
    
    func instantiateClassCustomViews(classes : [_class]) {
        

       // let  classController : CustomClassController?

        for _class in classes {
           let  classController = CustomClassController(frame: CGRect.zero)
            // set class data to class controller
            classController.myClass = _class
          //  print(classController.myClass)
            classController.updateview()
            classController.resonsibleController = self
            let gesture = UITapGestureRecognizer(target: self, action: #selector(classViewTapped(_:)))
            classController.view.addGestureRecognizer(gesture)
            // add class view to shcools stack view and add it's constrains
            schoolCoursesStackView.addArrangedSubview((classController.view)!)
            let constraint = NSLayoutConstraint(item: classController.view, attribute: .width, relatedBy: .equal, toItem: schoolCoursesStackView, attribute: .width, multiplier: 1, constant: -20)
            NSLayoutConstraint.activate([constraint])
        }
    }
    
    // MARK:- MY Subjects section
    /////////////////////////////////////////////////////////////////////
    
    func requestMySubjects(){
        
        activityIndicator2.isHidden = false
        serverError2.isHidden = true
    
        
        let parameters: [String: Any] = [
            "Data":
                [
                    "action": "MYSUBJECTS",
                    "lang": lang
            ],
            "Request":[
                "api_key":api_Key,
                "user_id":userID
                
            ]
        ]
        Alamofire.request(URL(string: rootUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ["Authorization": "Bearer " + api_Key]).responseJSON {[unowned self] (response) in
            if response.result.isSuccess {
                let jsonData = JSON(response.data!)
                if jsonData["Status"]["succeed"].intValue == 1 {
                    self.emptySubjects()
                    self.createMySubjectsArray(json: jsonData["Result"]["Subjects"].array!)
                    self.activityIndicator2.isHidden = true
                    self.refreshControll2.endRefreshing()
                }else{
                    
                    
                    // trying to retrive mysubjects from realm database
                    
                    if self.realmMySubjects() != nil {
                        self.activityIndicator2.isHidden = true
                        self.refreshControll2.endRefreshing()
                        self.emptySubjects()
                        self.mySubjects = self.realmMySubjects()!
                        self.instantiateSubjectCustomViews()
                        
                    }else{
                        print("failed")
                        self.emptySubjects()
                        self.refreshControll2.endRefreshing()
                        self.activityIndicator2.isHidden = true
                    }
                    
                    
                }
            }else{
                if self.realmMySubjects() != nil {
                    self.activityIndicator2.isHidden = true
                    self.refreshControll2.endRefreshing()
                    self.emptySubjects()
                    self.mySubjects = self.realmMySubjects()!
                    self.instantiateSubjectCustomViews()
                    
                }else{
                self.emptySubjects()
                self.activityIndicator2.isHidden = true
                self.serverError2.isHidden = false
                self.refreshControll2.endRefreshing()
                }
            }
        }
    }
    func emptySubjects (){
        for  view in mySubjectsStackView.arrangedSubviews{
            mySubjectsStackView.removeArrangedSubview(view)
            view.removeFromSuperview()
            
        }
    }
    
    func createMySubjectsArray (json : [JSON]){
        
        if json.count < 1 {
            return
        }
        
        var mySubject : MySubject
        for index in 0...json.count-1{
             mySubject = MySubject()
            mySubject.id = json[index]["id"].intValue
            mySubject.title = json[index]["title"].stringValue
            mySubject.termFullName = json[index]["term_full_name"].stringValue
            mySubject.imageUrl = json[index]["image_url"].stringValue
            mySubject.classTitleAr = json[index]["get_class"]["title_ar"].stringValue
            mySubject.classTitleEn = json[index]["get_class"]["title_en"].stringValue
            
            mySubjects.append(mySubject)
            mySubject.store()
        }
        
        instantiateSubjectCustomViews()
    }
    weak var mySubjectController :CustomMySubjectController?
    func instantiateSubjectCustomViews(){
        
        for subject in mySubjects {
            
             mySubjectController = CustomMySubjectController(frame: CGRect.zero)
            mySubjectController?.mySubject = subject
            //update subject view with subject details
            mySubjectController?.updateView()
            // add subject custom view to my subjects stack view
            mySubjectsStackView.addArrangedSubview((mySubjectController?.view)!)
            // set constrains to subject custom view
            let constraint1 = NSLayoutConstraint(item: mySubjectController!.view, attribute: .width, relatedBy: .equal, toItem: mySubjectsStackView, attribute: .width, multiplier: 1, constant: -20)
            let constraint = NSLayoutConstraint(item: mySubjectController!.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 160)
            NSLayoutConstraint.activate([constraint,constraint1])
            
//            let gesture = UITapGestureRecognizer(target: self, action: #selector(subjectViewTapped(_:)))
//            mySubjectController.view.addGestureRecognizer(gesture)
            mySubjectController?.subjectDetailsButton.addTarget(self, action: #selector(subjectViewTapped(_:)), for: .touchUpInside)
            
        }
    }
    // MARK:- Demo class section
    ///////////////////////////////////////////////////
    weak var demoClassController :DemoClassController?
    func instantiateDemoClassCustomView (classes : [_class]){
        
        
        for demoClass in classes {
            demoClassController = DemoClassController()
            demoClassController!.demoClass = demoClass
            // set this view controller as responsible controller for demo class
            demoClassController!.resonsibleController = self
            // update demo class view
            demoClassController!.updateView()
            // add demo class custom view to deno stack view
            demoSubjectStackView.addArrangedSubview(demoClassController!.view)
            // set constrains to subject custom view
            let constraint1 = NSLayoutConstraint(item: demoClassController!.view, attribute: .width, relatedBy: .equal, toItem: demoSubjectStackView, attribute: .width, multiplier: 1, constant: -20)
            let constraint = NSLayoutConstraint(item: demoClassController!.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 160)
            NSLayoutConstraint.activate([constraint,constraint1])
         // add gesture recongnizer for demo class
            let gesture = UITapGestureRecognizer(target: self, action: #selector(demoViewTapped(_:)))
            demoClassController!.view.addGestureRecognizer(gesture)
        }
    }
    @objc func demoViewTapped (_ sender:UITapGestureRecognizer){
        
        
     
            // call tapped function in demo class which tapped
            let demoController = sender.view?.resonsibleController as! DemoClassController
            demoController.tapped()

        
    }
    @objc func classViewTapped(_ sender:UITapGestureRecognizer) {
        
        let classController = sender.view?.resonsibleController as! CustomClassController
        classController.tapped()
    }
    
    
    @objc func subjectViewTapped (_ sender : UIButton){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subjectDetailsViewController = storyboard.instantiateViewController(withIdentifier: "SubjectDetails") as! SubjectDetailsViewController
        let subjectViewController = sender.superview?.resonsibleController as! CustomMySubjectController
   
        subjectDetailsViewController.subjectId = subjectViewController.mySubject!.id
        
        present(subjectDetailsViewController, animated: false, completion: nil)
    }
    
    func registerDemoSubjectController (demo : DemoSubjectController){
        demo.watchDemoButton.addTarget(self, action: #selector(self.watchDemoTapped(_:)), for: .touchUpInside)
        demo.playDemoButton.addTarget(self, action: #selector(self.playDemoTapped(_:)), for: .touchUpInside)
    }
    @objc func watchDemoTapped(_ sender : UIButton){
       
        let demoController = sender.resonsibleController as! DemoSubjectController
        demoController.watchDemoTapped()
    }
    
    @objc func playDemoTapped(_ sender : UIButton){
       // print("yyyyyyy")
        let demoController = sender.resonsibleController as! DemoSubjectController
        demoController.playLesson()
    }
    
    
    func getCurrentPage () -> Int {

            return Int(contentsScrollView.contentOffset.x / contentsScrollView.frame.size.width)
            
    }
    
    
    @IBAction func notificationPressed(_ sender: Any) {
        
        menuController.loadViewController(identifier: "Invoice", viewController: InvoicesViewController.instance)
    }
    
    

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        print("DDDDDDD")
        let x = scrollView.contentOffset.x
        print(x)
        let ratio = x/scrollView.frame.width
        let distanceToMove = ratio*selectionBar.frame.width
        selectionBar.transform = CGAffineTransform(translationX: distanceToMove, y: 0)
        selectionBar.layoutIfNeeded()
        //selectionBar.frame.x
    }
}
