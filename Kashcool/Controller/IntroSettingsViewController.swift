//
//  IntroSettingsViewController.swift
//  Kashcool
//
//  Created by aya on 10/29/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import SideMenu
import AVFoundation
class IntroSettingsViewController: UIViewController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func englishBtnPressed(_ sender: Any) {
        
        Language.setAppLanguage(lang: "en")
        lang = "en"
        UISideMenuNavigationController.lang = "en"
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        //NotificationCenter.default.post(name: .languageChanged, object: nil)
        
        playVideo(from: "video.mp4")
        (sender as! UIButton).isEnabled = false
    }
    
    
    @IBAction func arabicBtnPressed(_ sender: Any) {
        
        Language.setAppLanguage(lang: "ar")
        lang = "ar"
        UISideMenuNavigationController.lang = "ar"
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        //NotificationCenter.default.post(name: .languageChanged, object: nil)
        playVideo(from: "video.mp4")
        (sender as! UIButton).isEnabled = false
    }
    
    
    func playVideo(from file:String) {
        let file = file.components(separatedBy: ".")
        print(file)
        guard let path = Bundle.main.path(forResource: file[0], ofType:file[1]) else {
            debugPrint( "\(file.joined(separator: ".")) not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        
        self.view.layer.addSublayer(playerLayer)

        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        player.play()
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(playerDidFinishPlaying))
        self.view.addGestureRecognizer(gesture)
        
    }
    
    @objc func playerDidFinishPlaying() {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GetStarted")
        
        UIApplication.shared.keyWindow?.rootViewController = vc
        
       //  self.present(vc, animated: true, completion: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    

}
