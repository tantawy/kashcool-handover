//
//  HelpViewController.swift
//  Kashcool
//
//  Created by aya on 10/21/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    
    static weak var instance : HelpViewController?
    override func viewDidLoad() {
        super.viewDidLoad()

        if HelpViewController.instance == nil{
            HelpViewController.instance = self
        }
    }

    @IBAction func aboutUsPressed(_ sender: Any) {
        let webController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HelpWeb") as! HelpWebViewController
        webController.url = "https://kashcool.com.kw" + "/" + lang + "/mobile/aboutus"
        self.present(webController, animated: true, completion: nil)
    }
    
    @IBAction func termsPressed(_ sender: Any) {
        let webController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HelpWeb") as! HelpWebViewController
        webController.url = "https://kashcool.com.kw" + "/" + lang + "/mobile/term"
        self.present(webController, animated: true, completion: nil)
    }
    
    @IBAction func faqsPressed(_ sender: Any) {
        let webController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HelpWeb") as! HelpWebViewController
        webController.url = "https://kashcool.com.kw" + "/" + lang + "/mobile/faq"
        self.present(webController, animated: true, completion: nil)
    }
    
}
