//
//  AppDelegate.swift
//  Kashcool
//
//  Created by aya on 9/23/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Firebase
import SideMenu
import RealmSwift
let resourcesFolder = "Kashcool Resoursec"
//var currentViewContoller : UIViewController?
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate ,MessagingDelegate {
    
    var window: UIWindow?
    
    func unzipPlayer (url : URL){
        
        let resourceManager = ResourceManager()
        resourceManager.unzipFile(url: url, destination: url.deletingLastPathComponent(), password : nil) { (_)
            in
            resourceManager.removeFile(url: url)
        }
    }
    
    func checkPlayerFile ()-> Bool{
        let resourceManager = ResourceManager()
       return resourceManager.fileExist(fileName: nil, path: resourcesFolder + "/player/css")
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        lang = Language.currentLanguage()
        UISideMenuNavigationController.lang = Language.currentLanguage()
        
        
        let defauls = UserDefaults.standard
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController : UIViewController
        if let status = defauls.value(forKey: "Loaded Befre") as? String {
            if status != "YES"{

                 initialViewController = storyboard.instantiateViewController(withIdentifier: "Intro")
            }else{
                initialViewController = storyboard.instantiateViewController(withIdentifier: "SignIn")
            }
 
        }else{
            initialViewController = storyboard.instantiateViewController(withIdentifier: "Intro")
        }
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()

        let config = Realm.Configuration(

            schemaVersion: 16

        )
        Realm.Configuration.defaultConfiguration = config

      ////////////////////////////////////// player
        if !checkPlayerFile(){
        
        let resourceManager = ResourceManager()
        
        resourceManager.createDirectory(directory: resourcesFolder + "/player")
        
        resourceManager.downloadFile(url: "https://kashcool.com.kw/player/player.zip", in: resourcesFolder + "/player" , name: "player.zip", completion: {(url) in
            
            if url != nil{
                        self.unzipPlayer(url: url!)
            }
            
            } , _progress: nil)
        }
        //FirebaseApp.configure()
        //////////////////////////////////////////////
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

