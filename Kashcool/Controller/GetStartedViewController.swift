//
//  IntroViewController.swift
//  Kashcool
//
//  Created by aya on 10/29/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class GetStartedViewController: UIViewController {

    @IBOutlet weak var getStartedBtn: UIButton!
    
    
    @IBOutlet weak var discription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if lang == "ar" {
            discription.text = "تفاعل مع محتويات الماده التعليميه بشكل مثير ومشوق وجذاب وتوصيل المعلومات بخبره تعليميه مميزه"
        }
        getStartedBtn.cornerRadius = getStartedBtn.frame.height/2
    }
    

    @IBAction func getStartedPressed(_ sender: UIButton) {
        
        let signInController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignIn") as! SignInController
        self.present(signInController, animated: true, completion: nil)
        let defauls = UserDefaults.standard
        defauls.set("YES", forKey: "Loaded Befre")
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }

}
