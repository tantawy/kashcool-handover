//
//  SubjectDetailsViewController.swift
//  Kashcool
//
//  Created by aya on 10/1/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftHash
import SVProgressHUD
import Cosmos
import RealmSwift
class SubjectDetailsViewController: UIViewController {
    
    
    
    var didLoaded = false
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var subjectImage: UIImageView!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var initializeSettingsView: UIView!
    @IBOutlet var descriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var subjectTitle: UILabel!

    @IBOutlet weak var userRating: CosmosView!
    
    @IBOutlet weak var showMoreBtn: UIButton!
    @IBOutlet weak var subjectRate: CosmosView!
    @IBOutlet weak var subjectPriceText: UILabel!
    @IBOutlet weak var subjectTitle2: UILabel!
    @IBOutlet weak var classTitle: UILabel!
    @IBOutlet weak var termFullName: UILabel!
    @IBOutlet weak var settingsProgress: UIProgressView!
    
    @IBOutlet weak var DemoProgress: UIProgressView!
    @IBOutlet weak var downloadSettingsButton: UIButton!
    @IBOutlet weak var lessonsStackView: UIStackView!
    
    @IBOutlet weak var feedBackStackView: UIStackView!
    
    @IBOutlet weak var showAllView: UIView!
    
    @IBOutlet weak var watchDemoButton: UIButton!
    
    @IBOutlet weak var addToCartButton: UIButton!
    
    @IBOutlet weak var blockImage: UIImageView!
    
    @IBOutlet weak var errorView: UIView!
    
    var subjectDetails : SubjectDetails?
    var subjectId : Int?
    var isPurchased = true
    
    override func viewDidAppear(_ animated: Bool) {
        let scale = lang == "en" ? -1 : 1
        backBtn.transform = CGAffineTransform(scaleX: CGFloat(scale), y: 1)
        super.viewDidAppear(animated)
        if !didLoaded{
        SVProgressHUD.show()
        getData()
        }
        didLoaded = true
    }
    
    
    @IBAction func showMoreTapped(_ sender: UIButton) {
        
        if descriptionHeight.isActive {
            descriptionHeight.isActive = false
            descriptionText.sizeToFit()
            let newConstraint = NSLayoutConstraint(item: descriptionText, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: descriptionText.frame.height)
            NSLayoutConstraint.activate([newConstraint])
          showMoreBtn.setTitle(NSLocalizedString("Show less", comment: ""), for: .normal)
            
        }else {
            NSLayoutConstraint.activate([descriptionHeight])
            descriptionText.sizeToFit()
           showMoreBtn.setTitle(NSLocalizedString("Show more", comment: ""), for: .normal)
           
        }
    }
    
    
    // for theme
    @IBAction func downloadSettingsPressed(_ sender: UIButton) {
        
        downloadSettingsButton.isHidden = true
        settingsProgress.isHidden = false
        let resourceManager = ResourceManager()
        resourceManager.downloadFile(url: subjectDetails!.themeUrl, in: resourcesFolder + "/"
            + subjectDetails!.folderName , name: "theme.zip", completion: { (destinationUrl) in
                if destinationUrl != nil{
                resourceManager.unzipFile(url: destinationUrl!, destination: destinationUrl!.deletingLastPathComponent(), password: nil, completion: { (_) in
                    // do some nice animations 
                    self.watchDemoButton.isHidden = self.isPurchased
                    self.watchDemoButton.transform = CGAffineTransform(scaleX: 0, y: 0)
                    if self.isPurchased {
                    self.lessonsStackView.alpha = 0
                    }
                    UIView.animate(withDuration: 0.5, animations: {
                        self.initializeSettingsView.isHidden = true
                        self.lessonsStackView.isHidden = false
                    }, completion: { (completed) in
                        self.initializeSettingsView.isHidden = true
                        self.lessonsStackView.alpha = 1
                        if completed {
                            UIView.animate(withDuration: 0.3, animations: {
                                self.watchDemoButton.transform = CGAffineTransform.identity
                            })
                        }
                    })
                        self.initializeSettingsView.isHidden = true
                    resourceManager.removeFile(url: destinationUrl!)
                    
                })
                }else{
                    
                    // when no connection detination url will be nil
                    self.downloadSettingsButton.isHidden = false
                    self.settingsProgress.isHidden = true
                    
                    let title = lang == "ar" ? "خطا" : "Error"
                    let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connction"
                    self.showErrorMessage(title: title, message: message)
                    
                    
                }
        }) { (progress) in
            self.settingsProgress.progress = Float(progress)
        }
    }
    
    
    @IBAction func wathchDemoPressed(_ sender: UIButton) {
        if sender.tag == 0 {
            downloadDemoLesson()
            
        }else {
            
            let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let demoLessonUrl = documentsUrl.appendingPathComponent(resourcesFolder + "/" + subjectDetails!.folderName + "/DemoLessons/" + getDemoLessonFile())
           // print(demoLessonUrl.absoluteString)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let webViewController = storyboard.instantiateViewController(withIdentifier: "webView") as! WebViewController
           webViewController.fileUrl = demoLessonUrl
            present(webViewController, animated: true, completion: nil)
        }
    }
    
    func updateSubjectView(){
      //  print(subjectId)
        
        
        addToCartButton.isHidden = isPurchased
        if CartViewController.itemExist(id: subjectDetails!.id) {
            
            if lang == "ar"{
                addToCartButton.setTitle("اضيف الى السله", for: .normal)
            }else{
                addToCartButton.setTitle("Added To Cart", for: .normal)
            }
            addToCartButton.backgroundColor = UIColor.lightGray
            addToCartButton.isEnabled = false
        }
        
        if checkThemeFile() {
            initializeSettingsView.isHidden = true
            lessonsStackView.isHidden = false
            watchDemoButton.isHidden = isPurchased
        }else{
            initializeSettingsView.isHidden = false
            lessonsStackView.isHidden = isPurchased
        }
    
        updateDemoButton()
        
        subjectImage.sd_setImage(with: URL(string: subjectDetails!.imageUrl), completed: nil)
        subjectTitle.text = subjectDetails!.title
        subjectTitle2.text = subjectDetails!.title
        subjectPriceText.text = "\(subjectDetails!.price)"
        classTitle.text = subjectDetails!.className
        descriptionText.text = subjectDetails!.description
        termFullName.text = subjectDetails!.term == 1 ? "First term" : "Second term"
        subjectRate.rating = Double(subjectDetails!.rate)
       // userRating.didFinishTouchingCosmos = {(rating) in self.rateSubject(rate: Int(rating))}
    }
    
    func parseSubjectData (json : JSON){
        
        // parsing subject data
        
        let subject = SubjectDetails()
        subject.id = json["id"].intValue
        subject.title = json["title"].stringValue
        subject.folderName = json["folder_name"].stringValue
        subject._description = json["desc"].stringValue
        subject.price = json["price"].intValue
        subject.imageUrl = json["image"].stringValue
        subject.themeUrl = json["theme_url"].stringValue
        subject.className = json["class_name"].stringValue
        subject.userRate = json["userRate"].floatValue
        subject.rate = json["rate"].floatValue
        subject.term = json["term"].intValue
        subject.demoUrl = json["demo_encrypted_url"].stringValue
        
        
        
        // parsing unit data
        
        for unitJSON in json["units"].arrayValue{
            let unit = Unit()
            unit.id = unitJSON["id"].intValue
            unit.title = unitJSON["title"].stringValue
            
            // parsing lessons
            for lessonJSON in unitJSON["lessons"].arrayValue{
                
                let lesson = Lesson()
                lesson.id = lessonJSON["id"].intValue
                lesson.title = lessonJSON["title"].stringValue
                lesson.imageUrl = lessonJSON["image"].stringValue
                lesson.unitID = lessonJSON["unit_id"].stringValue
                lesson.url = lessonJSON["url"].stringValue
                lesson.lessonName = lessonJSON["lesson_name"].stringValue
                unit.lessons.append(lesson)
            }
            subject.units.append(unit)
            
        }
        
        // parsing feed backs
        for feedBackJson in json["feed_back"].arrayValue{
            let feedBack = FeedBack()
            feedBack.id = feedBackJson["id"].intValue
            feedBack.comment = feedBackJson["comment"].stringValue
            feedBack.date = feedBackJson["created_at"].stringValue
            feedBack.imageUrl = feedBackJson["imageUrl"].stringValue
            feedBack.userRate = feedBackJson["user_subject_rate"].intValue
            feedBack.userName = feedBackJson["full_name"].stringValue
            feedBack.userId = feedBackJson["user_id"].intValue
            subject.feedBacks.append(feedBack)
        }
        print("parsing finished")
        subjectDetails = subject
        subject.store()
        
    }
    weak var lessonController: LessonCustomController?
    func instantiateLessons (){
        
        for unit in subjectDetails!.units {
            
            for lesson in unit.lessons{
                
                 lessonController = LessonCustomController(frame: CGRect.zero)
                lessonController!.lesson = lesson
                lessonController?.resonsibleController = self
                lessonController!.downloadButton.addTarget(self, action: #selector(self.downloadButtonPressed(_:)), for: .touchUpInside )
                lessonController!.playButton.addTarget(self, action: #selector(self.playButtonPressed(_:)), for: .touchUpInside)
                lessonController!.subjectFolderName = subjectDetails?.folderName
                lessonController!.updateView(isPruchased: isPurchased)
                lessonsStackView.addArrangedSubview(lessonController!.view)
                
                let height = isPurchased ? 120 : 50
                let constraint = NSLayoutConstraint(item: lessonController!.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: CGFloat(height))
                let constraint2 = NSLayoutConstraint(item: lessonController!.view, attribute: .width, relatedBy: .equal, toItem: lessonsStackView, attribute: .width, multiplier: 1, constant: -40 )
                NSLayoutConstraint.activate([constraint,constraint2])
            }
        }
    }
    func instantiateFeedBacks (){
        if subjectDetails!.feedBacks.count > 0 {
            showAllView.isHidden = false
        }
        else{
            showAllView.isHidden = true
        }
        for feedBack in subjectDetails!.feedBacks {
            let feedBackController = FeedBackController(frame: CGRect.zero)
            feedBackController.feedBack = feedBack
          feedBackController.updateView()
            feedBackStackView.addArrangedSubview(feedBackController.view)
            
            let constraint = NSLayoutConstraint(item: feedBackController.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 128)
            let constraint2 = NSLayoutConstraint(item: feedBackController.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 240 )
            
            NSLayoutConstraint.activate([constraint,constraint2])

        }
    }
    
    
    func realmSubjectDetails () -> SubjectDetails? {
        let results = try! Realm().objects(SubjectDetails.self)
        let array = Array(results).filter({$0.language == lang && $0.userId == userID && $0.id == subjectId})
        return array.count > 0 ? array[0] : nil
    }
    
    func getData () {
        
        
        
        
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let currnetTime = formatter.string(from: date)
        print(currnetTime)
        let parameters : [String : Any] = [
            "Data":
                [
                    "action": "LessonBysubjectID",
                    "lang": lang
            ],
            "Request":
                [
                    "api_key": api_Key,
                    "user_id":userID,
                    "subject_id": subjectId! ,
                    "date":currnetTime
                    
            ]
        ]
        Alamofire.request(rootUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { [unowned self](response) in
            if response.result.isSuccess{
               // print(response)
                let jsonData = JSON(response.data!)
                if jsonData["Status"]["succeed"].intValue == 1 {
                    
                    self.parseSubjectData(json: jsonData["Result"]["subject"])
                    self.updateSubjectView()
                    self.createSubjectFolder()
                    self.instantiateLessons()
                    self.instantiateFeedBacks()
                    self.blockImage.isHidden = true
                    SVProgressHUD.dismiss()
                }else {
                    
                    // trying to retrive from realm database
                    
                    if let mySubject = self.realmSubjectDetails() {
                        self.subjectDetails = mySubject
                        self.updateSubjectView()
                        self.instantiateLessons()
                        self.instantiateFeedBacks()
                        self.blockImage.isHidden = true
                        SVProgressHUD.dismiss()
                    }else{
                        print("notsucceed")
                        // print(response)
                        self.errorView.isHidden = false
                        SVProgressHUD.dismiss()
                    }
                    
                }
            }else{
                
                if let mySubject = self.realmSubjectDetails(){
                    self.subjectDetails = mySubject
                    self.updateSubjectView()
                    self.instantiateLessons()
                    self.instantiateFeedBacks()
                    self.blockImage.isHidden = true
                    SVProgressHUD.dismiss()
                }else{
                print("cant get data")
                self.errorView.isHidden = false
                 SVProgressHUD.dismiss()
                }
            }
        }
        
    }
    
    // for lessons
    @objc func downloadButtonPressed(_ sender : UIButton){
        
       let lessonController = sender.resonsibleController as! LessonCustomController
        lessonController.downloadLesson()
        
    }
    
    @objc func playButtonPressed(_ sender : UIButton){
        
        let lessonController = sender.resonsibleController as! LessonCustomController
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let webViewController = storyboard.instantiateViewController(withIdentifier: "webView") as! WebViewController
        let lessonName = lessonController.lesson!.lessonName
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileUrl = documentsURL.appendingPathComponent(resourcesFolder + "/" + subjectDetails!.folderName + "/lessons/" + lessonName)
        webViewController.fileUrl = fileUrl
        present(webViewController, animated: true, completion: nil)
        
    }
    
    func createSubjectFolder (){
        
        let resourceManager = ResourceManager()
        resourceManager.createDirectory(directory: resourcesFolder + "/\(subjectDetails!.folderName)/lessons")
    }
    
    
    func checkThemeFile ()-> Bool {
        
        let resourceManager = ResourceManager()
        return resourceManager.fileExist(fileName: nil, path: resourcesFolder + "/" + subjectDetails!.folderName + "/theme/scripts")
        
    }
    
    func getDemoLessonFile() -> String {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let demoLessonUrl = documentsUrl.appendingPathComponent(resourcesFolder + "/" + subjectDetails!.folderName + "/DemoLessons")
        do {
            // Get the directory contents urls
            let directoryContents = try FileManager.default.contentsOfDirectory(at: demoLessonUrl, includingPropertiesForKeys: nil, options: [])
           // print(directoryContents)
            
            // filter to get only html
            let htmlFiles = directoryContents.filter{ $0.pathExtension == "html" }
            if htmlFiles.count > 0 {
                
                return htmlFiles[0].lastPathComponent
                
            }
            return ""
        } catch {
            return ""
        }
    }
    func updateDemoButton (){
        if getDemoLessonFile() == "" {
            
            if lang == "ar" {
                watchDemoButton.setTitle("تحميل الدرس التجريبى", for: .normal)
            }else{
                watchDemoButton.setTitle("Download Demo Lesson", for: .normal)
            }
            watchDemoButton.tag = 0
        }else{
            watchDemoButton.tag = 1
        }
    }
    func downloadDemoLesson(){
        print("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY")
        let resourceManager = ResourceManager()
        
        let lessonName = URL(string: subjectDetails!.demoUrl)!.lastPathComponent
        print(lessonName)
        resourceManager.downloadFile(url: subjectDetails!.demoUrl, in: resourcesFolder + "/" + subjectDetails!.folderName, name: lessonName, completion: { (destinationUrl) in
            
            if destinationUrl != nil{
                 self.unzipDemoLesson(url: destinationUrl!)
            }else{
                
                let title = lang == "ar" ? "خطا" : "Error"
                let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connction"
                self.showErrorMessage(title: title, message: message)
            }
           
        }) { (progress) in
            if progress > 0 {
                self.watchDemoButton.isHidden = true
                self.DemoProgress.isHidden = false
                self.DemoProgress.progress = Float(progress)
            }
        }
        
    }
    func unzipDemoLesson (url : URL){
        let str  = url.lastPathComponent.replacingOccurrences(of: ".zip", with: "")
       // print(str)
        let password = MD5(str).lowercased()
        
       // print(password)
        let resourceManager = ResourceManager()
        resourceManager.unzipFile(url: url, destination: url.deletingLastPathComponent(), password: password, completion: { (exttractedFile) in
            self.unzipSubfileOflesson(url: url.deletingLastPathComponent().appendingPathComponent(exttractedFile))
           // print(url.deletingLastPathComponent().appendingPathComponent(exttractedFile))
            resourceManager.removeFile(url: url)
            
        })
    }
    
    func unzipSubfileOflesson(url : URL){
        let resourceManager = ResourceManager()
        resourceManager.unzipFile(url: url, destination: url.deletingLastPathComponent().appendingPathComponent("DemoLessons"), password: nil) { (_) in
            // update demo button
            if lang == "ar" {
                self.watchDemoButton.setTitle("مشاهده الدرس التجريبى", for: .normal)
            }else{
                self.watchDemoButton.setTitle("Watch Demo Lesson", for: .normal)
            }
            self.watchDemoButton.isHidden = false
            self.watchDemoButton.tag = 1
            self.DemoProgress.isHidden = true
            
            resourceManager.removeFile(url: url)
        }
    }

    @IBAction func showAllButtonPressed(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let feedBackController = storyboard.instantiateViewController(withIdentifier: "FeedBackView") as! FeedBackViewController
        feedBackController.feedBacks = subjectDetails!.feedBacks
        
        let window = (UIApplication.shared.delegate as! AppDelegate).window
        
        UIView.transition(with: window!, duration: 0.5, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        
        present(feedBackController, animated: true, completion: nil)
        
    }
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    
    func rateSubject(rate : Int){
        print(rate)
    }
    
    @IBAction func addToCartPressed(_ sender: UIButton) {
//        
//        let cartItem = CartItem()
//        cartItem._class = subjectDetails!.className
//        cartItem.id = subjectDetails!.id
//        cartItem.price = subjectDetails!.price
//        cartItem.term = subjectDetails!.term == 0 ? "First term" : "Second term"
//        cartItem.title = subjectDetails!.title
//        cartItem.type = "subject"
//        
//        CartViewController.addItem(item: cartItem)
//        
//        
//        
//        if lang == "ar" {
//            addToCartButton.setTitle("اضيف الى السله", for: .normal)
//        }else{
//            addToCartButton.setTitle("Added To Cart", for: .normal)
//        }
//        addToCartButton.backgroundColor = UIColor.lightGray
//        addToCartButton.isEnabled = false
        sendPayment(id: subjectId!, type: "subject")
    }
    
    func sendPayment(id : Int , type :String) {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        var packageIDs = [String]()
        var subjectIDs = [String]()
        if type == "package"{
            packageIDs.append("\(id)")
        }else if type == "subject" {
            subjectIDs.append("\(id)")
        }
        
        let parameters: [String: Any] = [
            "Data":
                [
                    "action": "Payment",
                    "lang": lang
            ],
            "Request":
                [
                    "api_key":api_Key,
                    "user_id":userID,
                    "platform":"ios",
                    "subject_ids":subjectIDs,
                    "package_ids":packageIDs,
                    "payment_type":"knet"
                    
                    
            ]
        ]
        print(parameters)
        Alamofire.request(URL(string: "https://kashcool.com.kw/api")!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            print(response)
            let json = JSON(response.data)
            //print(json)
            self.showErrorMessage(title: "", message: json["Status"]["message"].stringValue)
            SVProgressHUD.dismiss()
        }
        
        
    }
    
    func showErrorMessage(title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = lang == "ar" ? "حسنا" : "Ok"
        let action = UIAlertAction(title: ok, style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true)
    }
    
    

    @IBAction func addCommentPressed(_ sender: Any) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Comment") as! CommentViewController
        vc.subjectId = subjectId!
        print(subjectId)
        self.present(vc, animated: true, completion: nil)
    }
    
    
}
