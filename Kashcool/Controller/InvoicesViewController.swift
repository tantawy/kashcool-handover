//
//  InvoicesViewController.swift
//  Kashcool
//
//  Created by aya on 10/14/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

//var invoiceVC : InvoicesViewController?

class InvoicesViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var invoicesCollectionView: UICollectionView!
    var invoices = [Invoice]()
    static weak var instance : InvoicesViewController?
    
    let refresControll = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if InvoicesViewController.instance == nil {
            print("set")
            InvoicesViewController.instance = self
        }
       menuController.currentController = self
        invoicesCollectionView.delegate = self
        invoicesCollectionView.dataSource = self
      //  currentViewContoller = self
        getData()
        
        invoicesCollectionView.addSubview(refresControll)
        refresControll.tintColor = .green
        refresControll.addTarget(self, action: #selector(getData), for: UIControl.Event.valueChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        invoices.removeAll()
        invoicesCollectionView.reloadData()
        getData()
    }



    

    @objc func getData(){
        
        
        Alamofire.request(rootUrl + "/v2/invoice", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["Authorization": "Bearer " + api_Key, "Content-Language" : lang]).responseJSON { (response) in
            if response.result.isSuccess {
                let json = JSON(response.data!)
                if json["status"]["succeed"].intValue == 1{
                    
                    if let dataJSON = json["result"]["data"].array {
                        self.parseData(json: dataJSON)
                        self.refresControll.endRefreshing()
                    }
                }
            }else {
                print("can't get data")
                self.refresControll.endRefreshing()
            }
        }
  
    }
    
    
    
    func parseData (json : [JSON]){
        
        invoices.removeAll()
        invoicesCollectionView.reloadData()
        
        for myjson in json    {
            
            let invoice = Invoice()
            invoice.date = myjson["date"].stringValue
            invoice.id = myjson["id"].intValue
            invoice.price = myjson["price"].intValue
            invoice.code = myjson ["purchased_by"]["code"].intValue
            invoice.status = myjson["status_text"].stringValue
            invoice.statusCode = myjson["status"].intValue
            invoices.append(invoice)
        }
        invoicesCollectionView.reloadData()
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = invoicesCollectionView.dequeueReusableCell(withReuseIdentifier: "InvoiceCell", for: indexPath) as! InvoiceCell
        let invoice = invoices[indexPath.item]
        cell.date.text = invoice.date
        cell.invoiceNumber.text = "\(invoice.id)"
        cell.price.text = "\(invoice.price) KWD"
        cell.status.text = invoice.status
        
        switch invoice.code {
        case 1:
            cell.invoiceImage.image = #imageLiteral(resourceName: "credit-card-6-64")
        case 2:
            cell.invoiceImage.image = #imageLiteral(resourceName: "gift")
        case 3:
            cell.invoiceImage.image = #imageLiteral(resourceName: "cart_green")
        default : break
        }
        
        if invoice.statusCode == 0 {
            cell.borderColor = UIColor(rgb: 0xA95B60)
        }else {
            
            let greenColor = UIColor(red: 40, green: 152, blue: 146)
            cell.borderColor = greenColor
        }
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
      return invoices.count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: invoicesCollectionView.bounds.width * 0.95, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(invoices[indexPath.item].id)
    }
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
}
