//
//  ViewController.swift
//  Kashcool
//
//  Created by aya on 9/23/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import SwiftyJSON
import SVProgressHUD
import RealmSwift
//var userFullName = ""
//var userId = 0
//var userMobile = ""
//var userEmail = ""
//var userApi_key = ""
//var userAvatar_url = ""
//var userClassID = 0

class SignInController : UIViewController {
    
    
    let loginURL = "https://kashcool.com.kw/api/v2/user/login"
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var RegisterButton: UIButton!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var forgetView: UIView!
    
    
    @IBOutlet weak var forgetEmailTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesigns()
        let defaults = UserDefaults.standard
        if let status = defaults.value(forKey: "loginStatus") as? String{
        if status == "login"{
            if let userName = defaults.value(forKey: "userName") as? String, let password = defaults.value(forKey: "password") as? String{
                userNameTextField.text = userName
                passwordTextField.text = password
                sendLoginRequest()
            }
            
            }
            
        }
        
        if lang == "ar"{
            userNameTextField.placeholder = "رقم الهاتف"
            passwordTextField.placeholder = "كلمه المرور"
            forgetEmailTextField.placeholder = "ايميل ..."
        }else{
            userNameTextField.placeholder = "Phone number"
            passwordTextField.placeholder = "Password"
            forgetEmailTextField.placeholder = "Email ..."
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
        @objc func dismissKeyboard() {
            
            view.endEditing(true)
        }

    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        
        if userNameTextField.text != "" && passwordTextField.text != ""{
            sendLoginRequest()

        }
        else if userNameTextField.text == "" {
            
            let message = lang == "ar" ? "من فضلك ادخل رقم الهاتف" : "Please enter phone number"
            showErrorMessage(title: "", message: message)
        }
        else{
            
            let message = lang == "ar" ? "من فضلك ادخل كلمه المرور" : "Please enter your password"
            showErrorMessage(title: "", message: message)
        }
        
   
    }
    
    func setupDesigns(){
        
     
        userNameView.layer.cornerRadius = 3
        passwordView.layer.cornerRadius = 3
        loginButton.layer.cornerRadius = 3
        RegisterButton.layer.cornerRadius = 3
        
        logo.layer.shadowColor = UIColor.darkGray.cgColor
        userNameView.layer.shadowColor = UIColor.darkGray.cgColor
        passwordView.layer.shadowColor = UIColor.darkGray.cgColor
        loginButton.layer.shadowColor = UIColor.darkGray.cgColor
        RegisterButton.layer.shadowColor = UIColor.darkGray.cgColor
        
        logo.layer.shadowOpacity = 0.1
        userNameView.layer.shadowOpacity = 0.1
        passwordView.layer.shadowOpacity = 0.1
        loginButton.layer.shadowOpacity = 0.1
        RegisterButton.layer.shadowOpacity = 0.1
        
        logo.layer.shadowRadius = 1
        userNameView.layer.shadowRadius = 1
        passwordView.layer.shadowRadius = 1
        loginButton.layer.shadowRadius = 1
        RegisterButton.layer.shadowRadius = 1
        
        logo.layer.shadowOffset = CGSize(width: 2, height: 5)
        userNameView.layer.shadowOffset = CGSize(width: 2, height: 5)
        passwordView.layer.shadowOffset = CGSize(width: 2, height: 5)
        loginButton.layer.shadowOffset = CGSize(width: 2, height: 5)
        RegisterButton.layer.shadowOffset = CGSize(width: 2, height: 5)
        
    }
    
    
    func sendLoginRequest(){
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        let parameters: [String: Any] = [
            "email" : userNameTextField.text!
            ,"password" : passwordTextField.text!
            ,"platform" : "ios"
            //,"firebase_token" : InstanceID.instanceID().token()!
        ]
        
        Alamofire.request(loginURL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON {[weak self] response in
                if response.result.isSuccess{
                    let json = JSON(response.data!)
                    if json["status"]["succeed"].intValue == 1 {
                        print(json)
                        SVProgressHUD.dismiss()
                        SVProgressHUD.setMaximumDismissTimeInterval(1)
                        SVProgressHUD.showSuccess(withStatus: "Login successful")
                        let defaults = UserDefaults.standard
                        defaults.set(self?.userNameTextField.text, forKey: "userName")
                        defaults.set(self?.passwordTextField.text, forKey: "password")
                        defaults.set("login", forKey: "loginStatus")
                        self?.setUserData(json: json)
                    }else{
                        SVProgressHUD.dismiss()
                        let message = lang == "ar" ? "تاكد من صحه البيانات المدخله" : "You have entered wrong data please try again"
                        self?.showErrorMessage(title: "", message: message)
                        
                    }
                    
                }else {
                    
                    
                    //trying to login from saved data
                    let defaults = UserDefaults.standard
                    if defaults.value(forKey: "loginStatus") as? String == "login"{
                        print(defaults.value(forKey: "loginStatus"))
                        if let user = self?.realmUserData(){
                            self?.setUserData(user: user)
                            SVProgressHUD.dismiss()
                        }else{
                            SVProgressHUD.dismiss()
                            let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connction"
                            self?.showErrorMessage(title: "", message: message)
                        }
                        
                    }else{
                        
                        SVProgressHUD.dismiss()
                        let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connction"
                        self?.showErrorMessage(title: "", message: message)
                    }
                    
                    
                    
                   
                }
        }
    }
    
    // check if user data is saved or not if is saved it will return it
    func realmUserData () -> UserData? {
        let realm = try! Realm()
        if let user = realm.object(ofType: UserData.self, forPrimaryKey: "last"){
            return user
        }else{
            return nil
        }
    }
    
    weak var  homeVC : UIViewController?
    
    // getting data from realm database if it exists
    func setUserData (user : UserData){
        fullName = user.fullName
        userID = user.userID
        email = user.email
        api_Key = user.api_Key
        avatarUrl = user.avatarUrl
        classID = user.classID
        mobile = user.mobile
        homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeRoot")
        present(homeVC!, animated: true, completion: nil)
    }
   func setUserData(json : JSON) {
    fullName = json["user"]["full_name"].stringValue
    userID = json["user"]["id"].intValue
    email = json["user"]["email"].stringValue
    api_Key = json["user"]["api_key"].stringValue
    avatarUrl = json["user"]["avatar_url"].stringValue
    classID = json["user"]["class_id"].intValue
    mobile = json["user"]["mobile"].stringValue
    // saving data to realm database for offline mode
    let userData = UserData()


        userData.fullName = fullName
        userData.userID = userID
        userData.email = email
        userData.api_Key = api_Key
        userData.avatarUrl = avatarUrl
        userData.classID = classID
        userData.mobile = mobile


    userData.store()
    
     homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeRoot")
    present(homeVC!, animated: true, completion: nil)
    homeVC = nil
    }
    
    func showErrorMessage(title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = lang == "ar" ? "حسنا" : "Ok"
        let action = UIAlertAction(title: ok, style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true)
    }
    
    

    @IBAction func forgotPasswordPressed(_ sender: Any) {
        
        forgetView.isHidden = false
    }
    
    
    
    @IBAction func emailMePressed(_ sender: Any) {
        if forgetEmailTextField.text == "" {
            let message = lang == "ar" ? "من فضلك ادخل الايميل" : "Please enter valid email"
            showErrorMessage(title: "", message: message)
        }else{
        sendEmail()
        }
    }
    func sendEmail(){
        
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        let parameters: [String: Any] = [
            "Data":
                [
                    "action": "ForgetPassword",
                    "lang": lang
            ],
            "Request":[
                "email": forgetEmailTextField.text

            ]
        ]
        Alamofire.request(rootUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { [unowned self] (response) in
            if response.result.isSuccess {
                let json = JSON(response.data!)
                if json["Status"]["succeed"].intValue == 1 {
                    SVProgressHUD.dismiss()
                    SVProgressHUD.setMaximumDismissTimeInterval(1)
                    SVProgressHUD.showSuccess(withStatus: "Email sent successfully")
                }else {
                    SVProgressHUD.dismiss()
                    let message = lang == "ar" ? "من فضلك تاكد من صحه الايميل" : "Check if email is correct !"
                    self.showErrorMessage(title: "", message: message)
                }
            }else{
                SVProgressHUD.dismiss()
                let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connection"
                self.showErrorMessage(title: "", message: message)
            }
        }
    }
    

    @IBAction func closePressed(_ sender: Any) {
        forgetView.isHidden = true
    }
    
}

