//
//  ChargeCodeContoller.swift
//  Kashcool
//
//  Created by aya on 10/16/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
//var chargeCodeContoller : ChargeCodeContoller?
class ChargeCodeContoller: UIViewController {

    
    static weak var instance : ChargeCodeContoller?
    
    @IBOutlet weak var codeText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if ChargeCodeContoller.instance == nil {
            ChargeCodeContoller.instance = self
        }
        menuController.currentController = self
       // currentViewContoller = self
        //chargeCodeContoller = self
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    


    @IBAction func submit(_ sender: Any) {
        
        if codeText.text == ""{return}
        
        SVProgressHUD.show()
        
        let parameters = [
            "Data":
            [
                "action": "CodeCharge",
                "lang": lang
            ],
            "Request":
            [
                "api_key":api_Key,
                "user_id":userID,
                "platform":"ios",
                "code":codeText.text!
            ]
        ]
        
        Alamofire.request(URL(string: rootUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if response.result.isSuccess{
                print(self.codeText.text!)
                let json = JSON(response.data!)
                print(json)
                SVProgressHUD.dismiss()
                print(json["Status"]["message"][0].stringValue)
                self.showErrorMessage(title: "", message: json["Status"]["message"][0].stringValue)
            }else{
                let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connection"
                self.showErrorMessage(title: "", message: "Please check your connection")
                SVProgressHUD.dismiss()
            }
        }

        
        
    }
    func showErrorMessage(title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = lang == "ar" ? "حسنا" : "Ok"
        let action = UIAlertAction(title: ok, style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true)
    }
    

}
