//
//  InvoiceCell.swift
//  Kashcool
//
//  Created by aya on 10/14/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class InvoiceCell: UICollectionViewCell {
    
    @IBOutlet weak var invoiceImage: UIImageView!
    @IBOutlet weak var invoiceNumber: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var price: UILabel!
}
