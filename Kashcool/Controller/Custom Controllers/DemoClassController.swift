//
//  DemoClassController.swift
//  Kashcool
//
//  Created by aya on 9/26/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class DemoClassController: UIView {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var demoClassImage: UIImageView!
    @IBOutlet weak var demoClassTitle: UILabel!
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    var demoClass : _class?
    var demoSubjects = [DemoSubject]()
    var demoSubjectsControllers = [DemoSubjectController]()
    var isShowingDemoSubjects = false
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("DemoClassCustomView", owner: self, options: nil)
        self.bounds = self.view.frame
        view.translatesAutoresizingMaskIntoConstraints = false

        // set this controller as the view responsible contoller to call target gesture when needed
        self.view.resonsibleController = self
        self.addSubview(view)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateView(){
        demoClassImage.sd_setImage(with: URL(string: (demoClass?.imageUrl)!), completed: nil)
        demoClassTitle.text = demoClass?.title
        
  
    }
    
    
    func tapped (){
        
        requestDemoSubjects()
        
    }

    // call a request to get demo subjects data for this controller specific demo class id
    func requestDemoSubjects (){
        
        if !isShowingDemoSubjects{
        if demoSubjects.isEmpty{
           // activityIndicatorView.isHidden = false
            SVProgressHUD.show()
            print("requesting")
            
           let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForResource = 0.1
            manager.session.configuration.timeoutIntervalForRequest = 0.1
         
        manager.request(URL(string: rootUrl + "/v2/class/\(demoClass!.id)/demo")!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: ["Authorization": "Bearer " + api_Key,"Content-Language" : lang ]).responseJSON { (response) in
            if response.result.isSuccess {
               // self.activityIndicatorView.isHidden = true
                SVProgressHUD.dismiss()
                
                let json =  JSON(response.data!)
                if json["status"]["succeed"].intValue == 1 {
                    self.createDemoSubjectsArray(json: json["demos"].array!)
                    
                    
                }
            }else{
                
                if let demo = HomeViewController.realmDemoSubjects(for_class: self.demoClass!.id) {
                    SVProgressHUD.dismiss()
                    self.presentDemosubjects(subjects: demo, below_with: self.view)
                }else{
                
                print("cant get data")
               // self.activityIndicatorView.isHidden = true
                SVProgressHUD.dismiss()
                let title = lang == "ar" ? "خطا" : "Error"
                let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connction"
                self.showErrorMessage(title: title, message: message)
                }
            }
            }
        }
        
        else {
            
            // if demo subjects called before and has set data it will call to present the pre setted demo subjects
           
            showDemoSubjects()
            }
            
        }
        else {
            hideDemoSubjects()
            activityIndicatorView.isHidden = true
        }
    }
    // MARK:- demo subjects section
    //////////////////////////////////////////////////////////////
    func createDemoSubjectsArray (json : [JSON]) {
        
        for index in 0...json.count-1 {
            
            let demoSubject = DemoSubject()
            demoSubject.id = json[index]["id"].intValue
            demoSubject.demoTitle = json[index]["demo_title"].stringValue
            demoSubject.imageUrl = json[index]["image_url"].stringValue
            demoSubject.subjectTitle = json[index]["subject_title"].stringValue
            demoSubject.demoUrl = json[index]["demo_url"].stringValue
            demoSubject.term = json[index]["term"].stringValue
            demoSubject._classID = demoClass!.id
            demoSubjects.append(demoSubject)
            demoSubject.store()
        }
        
        
            presentDemosubjects(subjects: demoSubjects, below_with: self.view)
        
    }

    
    func presentDemosubjects (subjects : [DemoSubject] , below_with element : Any) {
        
        var count = 1
        let controller = resonsibleController as! HomeViewController

        for demoSubject in subjects{
            
            
            let demoSubjectController = DemoSubjectController()
            demoSubjectController.demoSubject = demoSubject
            demoSubjectController.updateView()
            let homeVC = self.resonsibleController as! HomeViewController
            homeVC.registerDemoSubjectController(demo: demoSubjectController)
            controller.demoSubjectStackView.insertArrangedSubview(demoSubjectController.view, at: controller.demoSubjectStackView.arrangedSubviews.index(of: element as! UIView)! + count)
            //set constraints of demo subject view
            let constraint = NSLayoutConstraint(item: demoSubjectController.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 160)
            let constraint1 = NSLayoutConstraint(item: demoSubjectController.view, attribute: .width, relatedBy: .equal, toItem: controller.demoSubjectStackView, attribute: .width, multiplier: 1, constant: -60)
            NSLayoutConstraint.activate([constraint,constraint1])
            
            
            
            demoSubjectsControllers.append(demoSubjectController)
            count = count + 1
        }
        isShowingDemoSubjects = true
    }
    
    func hideDemoSubjects(){
        
        for demoController in demoSubjectsControllers {
            demoController.view.isHidden = true
        }
        isShowingDemoSubjects = false
    }
    
    func showDemoSubjects (){
        for demoController in demoSubjectsControllers {
            demoController.view.isHidden = false
        }
        isShowingDemoSubjects = true
    }
    
    func showErrorMessage(title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = lang == "ar" ? "حسنا" : "Ok"
        let action = UIAlertAction(title: ok, style: .cancel, handler: nil)
        alert.addAction(action)
        HomeViewController.instance?.present(alert, animated: true)
    }
    
    
    

}
