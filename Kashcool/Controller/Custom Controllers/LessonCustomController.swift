//
//  LessonCustomController.swift
//  Kashcool
//
//  Created by aya on 10/3/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import SwiftHash
class LessonCustomController: UIView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var downloadButton: UIButton!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var lessonTitle: UILabel!
    var lesson : Lesson?
    var subjectFolderName : String?
    
    @IBOutlet weak var downloadProgress: UIProgressView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("LessonCustomView", owner: self, options: nil)
        self.bounds = self.view.frame
        self.downloadButton.resonsibleController = self
        self.playButton.resonsibleController = self
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    func updateView (isPruchased : Bool){
        lessonTitle.text = lesson?.title
       // downloadButton.setTitle(NSLocalizedString("Download", comment: ""), for: .normal)
       // playButton.setTitle(NSLocalizedString("Play", comment: ""), for: .normal)
        if isPruchased{
        if checkLessonFile() {
            downloadButton.isHidden = true
            playButton.isHidden = false
        }
        }else{
            downloadButton.isHidden = true
        }
    }
    
    func playLesson(){
        
        
    }
    
    // called from subject details view controller
    func downloadLesson (){
        
        downloadButton.isHidden = true
        downloadProgress.isHidden = false
       let resourceManager = ResourceManager()
        resourceManager.downloadFile(url: (lesson?.url)!, in: resourcesFolder + "/\(subjectFolderName!)/lessons", name: lesson!.lessonName + ".zip", completion:{
            (url)  in
            
            if url != nil{
            self.unzipLesson(url: url!)
            }else{
                
                
                self.downloadButton.isHidden = false
                self.downloadProgress.isHidden = true
                
                let vc = self.resonsibleController as! SubjectDetailsViewController
                let title = lang == "ar" ? "خطا" : "Error"
                let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connction"
                vc.showErrorMessage(title: title, message: message)
            }
        } , _progress: {(progress) in
            
            self.downloadProgress.progress = Float(progress)
        })
    }
    
    
    func unzipLesson (url : URL) {
        let str  = URL(string: lesson!.url)!.lastPathComponent.replacingOccurrences(of: ".zip", with: "")
        
        let password = MD5(str).lowercased()
        print(password)

       let resourceManager = ResourceManager()
        resourceManager.unzipFile(url: url, destination: url.deletingLastPathComponent(), password: password, completion: { (exttractedFile) in
            
            self.unzipSubfileOflesson(url: url.deletingLastPathComponent().appendingPathComponent(exttractedFile))
            resourceManager.removeFile(url: url)
            
        })
        
    }
    
    func unzipSubfileOflesson(url : URL){
        let resourceManager = ResourceManager()
        resourceManager.unzipFile(url: url, destination: url.deletingLastPathComponent(), password: nil) { (_) in
            
            self.downloadProgress.isHidden = true
            self.playButton.isHidden = false
            resourceManager.removeFile(url: url)
        }
    }
    
    func checkLessonFile() -> Bool{
        
        let resourceManager = ResourceManager()
       return resourceManager.fileExist(fileName: lesson!.lessonName, path: resourcesFolder + "/\(subjectFolderName!)/lessons")
        
    }
    
    
    
}
