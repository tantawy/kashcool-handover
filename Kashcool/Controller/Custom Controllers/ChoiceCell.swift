//
//  ChoiceCell.swift
//  Kashcool
//
//  Created by aya on 10/23/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class ChoiceCell: UITableViewCell {

    @IBOutlet weak var answerLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
