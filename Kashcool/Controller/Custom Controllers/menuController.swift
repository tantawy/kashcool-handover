//
//  menuController.swift
//  Kashcool
//
//  Created by aya on 10/11/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import SideMenu
import SDWebImage

class menuController: UIViewController {
    static var invoiceVC : InvoicesViewController?
    static let navController = UINavigationController()
    static var currentController : UIViewController?
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var avatar: UIImageView!

    @IBOutlet weak var userEmail: UILabel!
    
    @IBOutlet weak var userName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("xxxxx")
        if avatarUrl != "https://kashcool.com.kw/uploadedimg/user/avatar.png" {
            avatar.sd_setImage(with: URL(string: avatarUrl), completed: nil)
        }
        userEmail.text = mobile
        userName.text = fullName
    }
   static func loadViewController (identifier : String , viewController : UIViewController?)
    {
        
        if viewController == nil {
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
            addViewController(viewController: vc)
            
            
        }else{
            print("found")
            addViewController(viewController: viewController!)
        }
        
        
        
    }
   static func addViewController (viewController : UIViewController){
        
        if (HomeViewController.instance?.navigationController?.viewControllers.contains(where : { return $0 == viewController }))!{
            if HomeViewController.instance?.navigationController?.viewControllers.last == viewController {
                
                return
            }
            print("push exist")
            let viewIndex = HomeViewController.instance?.navigationController?.viewControllers.index(of: viewController)
            
            HomeViewController.instance?.navigationController?.viewControllers.remove(at: viewIndex!)
            HomeViewController.instance?.navigationController?.pushViewController(viewController, animated: false)
            
            
        }else {
            print("push not exist")
            
            HomeViewController.instance?.navigationController?.pushViewController(viewController, animated: false)
            
        }
    }
    
    @IBAction func homeBtnPressed(_ sender: UIButton) {
        dismiss(animated: true) {
            
            HomeViewController.instance?.navigationController?.popToRootViewController(animated: false)
            
        }
        
    }
    
    
    @IBAction func invoicesBtnPressed(_ sender: UIButton) {
        
        dismiss(animated: true) {
            menuController.loadViewController(identifier: "Invoice", viewController: InvoicesViewController.instance)
            
        }
    }
    
    @IBAction func chargeCodeBtnPressed(_ sender: UIButton) {
        dismiss(animated: true) {
            menuController.loadViewController(identifier: "ChargeCode", viewController: ChargeCodeContoller.instance)
            
        }
    }
    
    @IBAction func QbBtnPressed(_ sender: UIButton) {
        dismiss(animated: true) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QB")
            HomeViewController.instance?.present(vc, animated: false, completion: nil)
            
        }
        
        
    }
    
    @IBAction func cartBtnPressed(_ sender: UIButton) {
        dismiss(animated: true) {
            menuController.loadViewController(identifier: "Cart", viewController: CartViewController.instance)
            
        }
    }
    @IBAction func settengsBtnPressed(_ sender: UIButton) {
        
        dismiss(animated: true) {
            menuController.loadViewController(identifier: "Settings", viewController: SettingsViewController.instance)
            
        }
    }
    
    @IBAction func helpBtnPressed(_ sender: UIButton) {
        dismiss(animated: true) {
            menuController.loadViewController(identifier: "Help", viewController: HelpViewController.instance)
            
    }
    }
    @IBAction func aboutUsBtnPressed(_ sender: UIButton) {
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func reportBtnPressed(_ sender: UIButton) {
        dismiss(animated: true) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Report")
            HomeViewController.instance?.present(vc, animated: false, completion: nil)
            
        }
    }
    @IBAction func logoutBtnPressed(_ sender: UIButton) {
        dismiss(animated: true) {
            let title = lang == "ar" ? "تسجيل الخروج" : "Logout"
            let message = lang == "ar" ? "هل تريد تسجيل الخروج ؟" : "Are you sure you want to logout ?"
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let logout = lang == "ar" ? "تسجيل الخروج" : "Logout"
            let action1 = UIAlertAction(title: logout, style: .default, handler: { (_) in
                
                let defaults = UserDefaults.standard
                defaults.removeObject(forKey: "userName")
                defaults.removeObject(forKey: "password")
                defaults.set("logout", forKey: "loginStatus")
                HomeViewController.instance?.navigationController?.dismiss(animated: true, completion: nil)
                HomeViewController.instance = nil
            })
            let cancel = lang == "ar" ? "الغاء" : "Cancel"
            let action2 = UIAlertAction(title: cancel, style: .cancel, handler: nil)
            alert.addAction(action2)
            alert.addAction(action1)
            HomeViewController.instance?.present(alert , animated: true)
            
        }
        
    }
    
    
    
    
}

