//
//  SubjectCustomController.swift
//  Kashcool
//
//  Created by aya on 9/27/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class SubjectCustomController: UIView {

    @IBOutlet var view: UIView!
    @IBOutlet weak var subjectImage: UIImageView!
    @IBOutlet weak var subjectTitle: UILabel!
    @IBOutlet weak var rateImage: UIImageView!
    @IBOutlet weak var classTitle: UILabel!
    @IBOutlet weak var termFullName: UILabel!
    @IBOutlet weak var subjectPrice: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    
    var _classTitle = ""
    var subject : Subject?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("SubjectCustomView", owner: self, options: nil)
        self.bounds = self.view.frame
        view.translatesAutoresizingMaskIntoConstraints = false
        
        // set this controller as the view responsible contoller to call target gesture when needed
        self.view.resonsibleController = self
        self.addToCartButton.resonsibleController = self
        self.addSubview(view)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func updateView(){
        
        subjectTitle.text = subject?.title
        subjectImage.sd_setImage(with: URL(string: (subject?.imageUrl)!), completed: nil)
        classTitle.text = _classTitle
        termFullName.text = subject?.termFullName
        subjectPrice.text = "\(subject!.price)" + "KWD"
        addToCartButton.isHidden = subject?.download == 1
        
        if CartViewController.itemExist(id: subject!.id) {

            if lang == "ar" {
                addToCartButton.setTitle("اضيف الى السله", for: .normal)
            }else{
                addToCartButton.setTitle("Added To Cart", for: .normal)
            }
            addToCartButton.backgroundColor = UIColor.lightGray
            addToCartButton.isEnabled = false
        }
    }
    
    
    func addTocart (){
//        CartViewController.addSubject(subject: subject!)
//        
//        
//        if lang == "ar" {
//            addToCartButton.setTitle("اضيف الى السله", for: .normal)
//        }else{
//            addToCartButton.setTitle("Added To Cart", for: .normal)
//        }
//        addToCartButton.backgroundColor = UIColor.lightGray
//        addToCartButton.isEnabled = false
      //  CartViewController.sendPayment(id: ((subject?.id)!), type: "subject")
    }
}
