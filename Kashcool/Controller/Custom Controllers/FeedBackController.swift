//
//  FeedBackController.swift
//  Kashcool
//
//  Created by aya on 10/9/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class FeedBackController: UIView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var rateImage: UIImageView!
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var comment: UILabel!
    
    var feedBack : FeedBack?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("FeedBackView", owner: self, options: nil)
        self.bounds = self.view.frame
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func updateView (){
        
        if feedBack?.imageUrl == "https://kashcool.com.kw/uploadedimg/user/avatar.png"{
            userImage.image = #imageLiteral(resourceName: "default-user")
        }
        else{
        userImage.sd_setImage(with: URL(string: feedBack!.imageUrl), completed: nil)
        }
        userImage.layer.cornerRadius = userImage.frame.width/2
        
        userImage.clipsToBounds = true
        if feedBack!.userName == "" {
            userName.text = "Unknown"
        }else{
            userName.text = feedBack!.userName
        }
        date.text = feedBack!.date
        comment.text = feedBack!.comment
        
    }
}
