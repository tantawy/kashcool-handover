//
//  CustomLabel.swift
//  Kashcool
//
//  Created by aya on 10/23/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {

    
    @IBInspectable var EngishText : String {
        
        set(value){
            english = value
        }
        get{
            return english
        }
    }
    @IBInspectable var ArabicText : String {
        
        set(value){
            arabic = value
        }
        get{
            return arabic
        }
    }
    
    private var arabic = ""
    private var english = ""
    
    @objc func localize(){
        
        self.text = ArabicText
        if lang == "ar"{
            self.text = ArabicText
        }else{
            self.text = EngishText
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        localize()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(self.localize),
                                       name: .languageChanged,
                                       object: nil)
    }
    
    

}
