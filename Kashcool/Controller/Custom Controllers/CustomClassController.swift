//
//  CustomClassController.swift
//  Kashcool
//
//  Created by aya on 9/24/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import SDWebImage
class CustomClassController : UIView {
    
    var myClass : _class?
    
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var classImage: UIImageView!
    
    @IBOutlet weak var classTitle: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Load and set custom class view from xib file
        Bundle.main.loadNibNamed("CustomClassView", owner: self, options: nil)
        self.bounds = self.view.frame
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        // make some round shape for the view
        view.layer.cornerRadius = 12
        classImage.layer.cornerRadius = 12
        classImage.clipsToBounds = true
        // add shadow
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.5, height: 1.5)
        view.layer.shadowRadius = 1
        view.layer.shadowOpacity = 1
        
        
        view.resonsibleController = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func updateview(){
        classTitle.text = myClass?.title
        classImage.sd_setImage(with: URL(string: (myClass?.imageUrl)!), completed: nil)
        
    }
    
    func tapped(){
      
        let viewController = resonsibleController as! UIViewController
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subjectsOfClassViewController = storyboard.instantiateViewController(withIdentifier: "vv") as! SubjectsOfClassViewController
        print(myClass)
        subjectsOfClassViewController.classID = myClass!.id
        subjectsOfClassViewController._classTitle = myClass!.title



        let window = (UIApplication.shared.delegate as! AppDelegate).window

        UIView.transition(with: window!, duration: 0.5, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        viewController.present(subjectsOfClassViewController, animated: false, completion: nil)
        
    }
}
