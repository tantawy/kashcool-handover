//
//  DemoSubjectController.swift
//  Kashcool
//
//  Created by aya on 9/26/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import SwiftHash
class DemoSubjectController: UIView {
    @IBOutlet var view: UIView!
    @IBOutlet weak var demoImageView: UIImageView!
    @IBOutlet weak var demoTitle: UILabel!
    @IBOutlet weak var subjectTitle: UILabel!
    @IBOutlet weak var watchDemoButton: UIButton!
    
    @IBOutlet weak var playDemoButton: UIButton!
    
    var demoSubject : DemoSubject?
    
    @IBOutlet weak var progressView: UIProgressView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("DemoSubjectCustomView", owner: self, options: nil)
        self.bounds = self.view.frame
        view.translatesAutoresizingMaskIntoConstraints = false
        // set this controller as the view responsible contoller to call target gesture when needed
        self.view.resonsibleController = self
        watchDemoButton.resonsibleController = self
        playDemoButton.resonsibleController = self
        self.addSubview(view)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    
    func updateView () {
        demoImageView.sd_setImage(with: URL(string: (demoSubject?.imageUrl)!), completed: nil)
        demoTitle.text = demoSubject?.demoTitle
        subjectTitle.text = demoSubject?.subjectTitle
        createFolder()
        
        if getDemoLessonFile() == "" {
            watchDemoButton.isHidden = false
            playDemoButton.isHidden = true
        }else{
            watchDemoButton.isHidden = true
            playDemoButton.isHidden = false
        }

    }
    func createFolder (){
        let resourceManager = ResourceManager()
        resourceManager.createDirectory(directory: resourcesFolder + "/" + (URL(string: demoSubject!.demoUrl)!.lastPathComponent))
    }
    
    func watchDemoTapped (){
        watchDemoButton.isEnabled = false
        downloadDemoLesson()
    }
    
    func downloadDemoLesson(){
        
        let resourceManager = ResourceManager()
        
        let lessonName = URL(string: demoSubject!.demoUrl)!.lastPathComponent
        print(lessonName)
        resourceManager.downloadFile(url: demoSubject!.demoUrl, in: resourcesFolder + "/" + (URL(string: demoSubject!.demoUrl)!.lastPathComponent), name: lessonName + ".zip", completion: { (destinationUrl) in
            if destinationUrl != nil {
                self.unzipDemoLesson(url: destinationUrl!)
            }else {
                
                // if there was no connection it will pass distination url as nil value
                
                
                print("watch demo un available")
                self.watchDemoButton.isEnabled = true
                let title = lang == "ar" ? "خطا" : "Error"
                let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connction"
                self.showErrorMessage(title: title, message: message)
                
            }
            
        }) { (progress) in
            if progress > 0 {
                self.watchDemoButton.isHidden = true
                self.progressView.isHidden = false
                self.progressView.progress = Float(progress)
            }
            
        }
        
    }
    
    func unzipDemoLesson (url : URL){
        
        
        let str  = url.lastPathComponent.replacingOccurrences(of: ".zip", with: "")
        let encodedSTR = Data(str.utf8).base64EncodedString()
        print(encodedSTR)
        let password = MD5(encodedSTR).lowercased()
        
        print(password)
        let resourceManager = ResourceManager()
        resourceManager.unzipFile(url: url, destination: url.deletingLastPathComponent(), password: password, completion: { (exttractedFile) in
            print(url.deletingLastPathComponent().appendingPathComponent(exttractedFile))
            resourceManager.removeFile(url: url)
            self.progressView.isHidden = true
            self.playDemoButton.isHidden = false
            //self.playLesson()

        })
    }
    func playLesson (){
        
       // playDemoButton.isEnabled = false
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let webViewController = storyboard.instantiateViewController(withIdentifier: "webView") as! WebViewController
        let lessonName = URL(string: demoSubject!.demoUrl)!.lastPathComponent
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileUrl = documentsURL.appendingPathComponent(resourcesFolder + "/" + lessonName + "/t\(demoSubject!.term)/" + getDemoLessonFile())
        
        webViewController.fileUrl = fileUrl
        HomeViewController.instance?.present(webViewController, animated: true, completion: nil)
    }
    
    func getDemoLessonFile() -> String {
        let lessonName = URL(string: demoSubject!.demoUrl)!.lastPathComponent
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
       // demoSubject.ter
        let demoLessonUrl =  documentsUrl.appendingPathComponent(resourcesFolder + "/" + lessonName + "/t\(demoSubject!.term)" )
        do {
            // Get the directory contents urls
            let directoryContents = try FileManager.default.contentsOfDirectory(at: demoLessonUrl, includingPropertiesForKeys: nil, options: [])
            print(directoryContents)
            
            // filter to get only html
            let htmlFiles = directoryContents.filter{ $0.pathExtension == "html" }
            if htmlFiles.count > 0 {
                
                return htmlFiles[0].lastPathComponent
                
            }
            return ""
        } catch {
            return ""
        }
    }
    
    func showErrorMessage(title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = lang == "ar" ? "حسنا" : "Ok"
        let action = UIAlertAction(title: ok, style: .cancel, handler: nil)
        alert.addAction(action)
        HomeViewController.instance?.present(alert, animated: true)
    }
}
