//
//  CustomButton.swift
//  Kashcool
//
//  Created by aya on 10/23/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    
    @IBInspectable var EngishText : String {
        
        set(value1){
            english = value1
        }
        get{
            return english
        }
    }
    @IBInspectable var ArabicText : String {
        
        set(value){
            arabic = value
        }
        get{
            return arabic
        }
    }
    
    private var english = ""
    private var arabic = ""
    
    @objc func localize(){
       
        if lang == "ar"{
        self.setTitle(ArabicText, for: .normal)
            if  self.contentHorizontalAlignment == .left{
            self.contentHorizontalAlignment = .right
            }
        }else{
            self.setTitle(EngishText, for: .normal)
            if  self.contentHorizontalAlignment == .right{
                self.contentHorizontalAlignment = .left
            }
        }
        
       
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        localize()
                let notificationCenter = NotificationCenter.default
                notificationCenter.addObserver(self,
                                               selector: #selector(self.localize),
                                               name: .languageChanged,
                                               object: nil)
    }
    
    


}
