//
//  CustomMySubjectController.swift
//  Kashcool
//
//  Created by aya on 9/25/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
class CustomMySubjectController : UIView {
    
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var subjectImage: UIImageView!
    @IBOutlet weak var subjectTitle: UILabel!
    
    @IBOutlet weak var termFullName: UILabel!
    @IBOutlet weak var classTitle: UILabel!
    @IBOutlet weak var subjectDetailsButton: UIButton!
    weak var  mySubject : MySubject?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Load and set custom class view from xib file
        Bundle.main.loadNibNamed("CustomMySubjectView", owner: self, options: nil)
        self.bounds = self.view.frame
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        // make some round shape for the view
        view.layer.cornerRadius = 8
        subjectImage.layer.cornerRadius = 12
        subjectDetailsButton.layer.cornerRadius = 5
        subjectImage.clipsToBounds = true
        // add shadow
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.5, height: 1.5)
        view.layer.shadowRadius = 2
        view.layer.shadowOpacity = 0.7
        
        
        view.resonsibleController = self
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func updateView(){
        
        
       
        subjectImage.sd_setImage(with: URL(string: (mySubject?.imageUrl)!), completed: nil)
        subjectTitle.text = mySubject?.title
        termFullName.text = mySubject?.termFullName
        if lang == "ar"{
            classTitle.text = mySubject?.classTitleAr
        }else{
        classTitle.text = mySubject?.classTitleEn
    }
    }
}
