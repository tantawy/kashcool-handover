//
//  CartCell.swift
//  Kashcool
//
//  Created by aya on 10/21/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet weak var itemTitle: UILabel!
    
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var details: UILabel!
    
    
    var item : CartItem?
    
    @IBAction func deleteItem(_ sender: Any) {
        
        
        CartViewController.removeItem(item: item!)
    }
    
    
}
