//
//  PackageCustomController.swift
//  Kashcool
//
//  Created by aya on 9/27/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class PackageCustomController: UIView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var packageImage: UIImageView!
    @IBOutlet weak var packageTitle: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var originalPrice: UILabel!
    @IBOutlet weak var classTitle: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    
    
    var package : Package?
    var _classTitle = ""
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("PackageCustomView", owner: self, options: nil)
        self.bounds = self.view.frame
        view.translatesAutoresizingMaskIntoConstraints = false
        
        // set this controller as the view responsible contoller to call target gesture when needed
        self.view.resonsibleController = self
        self.addToCartButton.resonsibleController = self
        self.addSubview(view)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func updateView(){
        
        packageImage.sd_setImage(with: URL(string: (package?.imageUrl)!), completed: nil)
        packageTitle.text = package?.name
        price.text = "\(package!.price) KWD"
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(package!.originalPrice) KWD")
        attributeString.addAttribute(.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        
        originalPrice.attributedText = attributeString
        classTitle.text = _classTitle
        
        if CartViewController.itemExist(id: package!.id) {
//            addToCartButton.setTitle(NSLocalizedString("Added To Cart", comment: ""), for: .normal)
            addToCartButton.backgroundColor = UIColor.lightGray
            addToCartButton.isEnabled = false
        }else {
            
           // addToCartButton.setTitle(NSLocalizedString("Add to cart", comment: ""), for: .normal)
        }
        
        
    }
    
    
    func addTocart (){
        
//        CartViewController.addPackage(package: package!)
//        
//        if CartViewController.itemExist(id: package!.id) {
//
//            if lang == "ar" {
//                addToCartButton.setTitle("اضيف الى السله", for: .normal)
//            }else{
//                addToCartButton.setTitle("Added To Cart", for: .normal)
//            }
//            addToCartButton.backgroundColor = UIColor.lightGray
//            addToCartButton.isEnabled = false
//        }
    }

}
