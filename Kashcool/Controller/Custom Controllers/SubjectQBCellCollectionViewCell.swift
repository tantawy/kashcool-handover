//
//  SubjectQBCellCollectionViewCell.swift
//  Kashcool
//
//  Created by aya on 10/23/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit

class SubjectQBCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var subjectImage: UIImageView!
    
    @IBOutlet weak var subjectTitle: UILabel!
    
    @IBOutlet weak var term: UILabel!
    
    @IBOutlet weak var _class: UILabel!
    
    @IBOutlet weak var startExamBtn: CustomButton!
    var subjectId = 0
    
}
