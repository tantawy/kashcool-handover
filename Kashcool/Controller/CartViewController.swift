//
//  CartViewController.swift
//  Kashcool
//
//  Created by aya on 10/21/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
class CartViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    static weak var instance : CartViewController?
    static var cartItems = [CartItem]()
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var totalPriceLabel: CustomLabel!
    static var myString = "Total price"
    static var totalPrice : Float = 0
    @IBOutlet weak var emptyView: UIView!
    
    @IBOutlet weak var codeTextField: UITextField!
    
    @IBOutlet weak var dicountView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if CartViewController.instance == nil {
            CartViewController.instance = self
        }
        CartViewController.myString = lang == "ar" ? "الاجمالى :" : "Total price :"
        CartViewController.instance?.totalPriceLabel.text = CartViewController.myString + "\(CartViewController.totalPrice)"
        
        table.estimatedRowHeight = 44
        table.rowHeight = UITableView.automaticDimension
        table.delegate = self
        table.dataSource = self
        CartViewController.reloadData()
        
       let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }


    static func addPackage(package : Package){
        
        let cartIem = CartItem()
        cartIem._class = package._class
        cartIem.id = package.id
        cartIem.term = package.term
        cartIem.title = package.name
        cartIem.type = "package"
        cartIem.price = package.price
        totalPrice = totalPrice + Float(package.price)
        myString = lang == "ar" ? "الاجمالى :" : "Total price :"
        CartViewController.instance?.totalPriceLabel.text = myString + "\(totalPrice)"
        cartItems.append(cartIem)
         reloadData()
        
    }
    
    
    static func addSubject(subject : Subject){
        
        let cartIem = CartItem()
        cartIem._class = subject._class
        cartIem.id = subject.id
        cartIem.term = subject.termFullName
        cartIem.title = subject.title
        cartIem.type = "subject"
        cartIem.price = subject.price
        totalPrice = totalPrice + Float(subject.price)
         myString = lang == "ar" ? "الاجمالى :" : "Total price :"
        CartViewController.instance?.totalPriceLabel.text = myString + "\(totalPrice)"
        cartItems.append(cartIem)
        reloadData()
        
    }
    
    static func addItem(item : CartItem){
        cartItems.append(item)
        reloadData()
        
    }

    
    
    static func removeItem (item : CartItem){
        if let index = cartItems.index(where: {$0.id == item.id}){
            cartItems.remove(at: index)
            totalPrice = totalPrice - Float(item.price)
            myString = lang == "ar" ? "الاجمالى :" : "Total price :"
            CartViewController.instance?.totalPriceLabel.text = myString + "\(totalPrice)"
            reloadData()
        }
        reloadData()
    }
    
    
    static func itemExist(id : Int) -> Bool {
       return cartItems.contains(where: {$0.id == id})
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
       // print(CartViewController.cartItems.count + 1)
        return CartViewController.cartItems.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.item == 0{
            let cell : UITableViewCell
            cell = table.dequeueReusableCell(withIdentifier: "header")!
            return cell
            
        }else{
            
           let cell2 = table.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
            print(CartViewController.cartItems.count)
            print(indexPath.item)
            cell2.price.text = "\(CartViewController.cartItems[indexPath.item - 1].price) KWD"
            cell2.details.text = "\(CartViewController.cartItems[indexPath.item - 1].term) |  \(CartViewController.cartItems[indexPath.item - 1]._class)"
            cell2.itemTitle.text = "\(CartViewController.cartItems[indexPath.item - 1].title)"
            cell2.item = CartViewController.cartItems[indexPath.item - 1]
            return cell2
        }
    }
    
    
    static func reloadData (){
        
        if CartViewController.instance != nil {
            
            if cartItems.count == 0 {
                
                CartViewController.instance?.emptyView.isHidden = false
            }else{
                
                CartViewController.instance?.emptyView.isHidden = true
                CartViewController.instance?.table.reloadData()
                
                
            }
        }
    }
    
    static func sendPayment() {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        var packageIDs = [String]()
        var subjectIDs = [String]()
        
        for item in cartItems {
            
            if item.type == "package"{
                packageIDs.append("\(item.id)")
            }else{
                subjectIDs.append("\(item.id)")
            }
        }
        print(subjectIDs)
        print(packageIDs)
        let parameters: [String: Any] = [
            "Data":
            [
                "action": "Payment",
                "lang": lang
            ],
            "Request":
            [
                "api_key":api_Key,
                "user_id":userID,
                "platform":"ios",
                "subject_ids":subjectIDs,
                "package_ids":packageIDs,
                "payment_type":"knet"


            ]
        ]
        print(parameters)
        Alamofire.request(URL(string: "https://kashcool.com.kw/api")!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            print(response)
            let json = JSON(response.data)
            //print(json)
            CartViewController.instance?.showErrorMessage(title: "", message: json["Status"]["message"].stringValue)
            SVProgressHUD.dismiss()
        }


    }
    
    func showErrorMessage(title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = lang == "ar" ? "حسنا" : "Ok"
        let action = UIAlertAction(title: ok, style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true)
    }
    
    @IBAction func proceedPressed(_ sender: Any) {
        
        CartViewController.sendPayment()
    }
    
    @IBAction func validatePressed(_ sender: Any) {
        checkPromotionCode()
    }
    
      func checkPromotionCode (){
      SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        Alamofire.request(URL(string: rootUrl + "/v2/payment/check-promotion-code/" + codeTextField.text!)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            CartViewController.instance?.dicountView.isHidden = true
            if response.result.isSuccess{
                let json = JSON(response.data!)
            if json["status"]["succeed"].intValue == 1 {
                SVProgressHUD.dismiss()
                if json["result"]["type_text"].stringValue == "value"{
                    CartViewController.totalPrice = CartViewController.totalPrice - json["result"]["value"].floatValue
                    self.totalPriceLabel.text = "\(CartViewController.totalPrice)"
                    
                    
                }else{
                    CartViewController.totalPrice = CartViewController.totalPrice - CartViewController.totalPrice * json["result"]["value"].floatValue
                    self.totalPriceLabel.text = "\(CartViewController.totalPrice)"
                }
            }else{
                SVProgressHUD.dismiss()
            }
            }else{
                SVProgressHUD.dismiss()
            }
            
        }
    }
    @IBAction func closeDiscount(_ sender: Any) {
       CartViewController.instance?.dicountView.isHidden = true
    }
    
    @IBAction func discountCodePressed(_ sender: Any) {
        
        CartViewController.instance?.dicountView.isHidden = false
        
    }
    
    
}
