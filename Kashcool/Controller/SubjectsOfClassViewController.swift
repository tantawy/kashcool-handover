//
//  subjectsOfClassControllerViewController.swift
//  Kashcool
//
//  Created by aya on 9/27/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import RealmSwift
class SubjectsOfClassViewController: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var subjectsScrollView: UIScrollView!
    
    @IBOutlet weak var subjectsStackView: UIStackView!
    var classID : Int?
    var _classTitle : String?
    
    @IBOutlet weak var noConnectionImage: UIImageView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var textTest: UITextView!
    var subjects = [Subject]()
    var packages = [Package]()
    
    @IBOutlet weak var classTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let scale = lang == "en" ? -1 : 1
        backBtn.transform = CGAffineTransform(scaleX: CGFloat(scale), y: 1)
        classTitle.text = _classTitle
        // delay till the transition end
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
           
            self.requestSubjectsData()
        }
        
        
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        for  view in subjectsStackView.arrangedSubviews{
//            subjectsStackView.removeArrangedSubview(view)
//            view.removeFromSuperview()
//            
//        }
//        instantiatePackagesCustomView()
//        instantiateSubjectCustomViews()
        print("xxxxxxx")
        print(packages.count)
        print(animated)
        
    }
    
    
    func requestSubjectsData (){
        
        SVProgressHUD.show()
        Alamofire.request(URL(string: rootUrl + "/v2/class/\(classID!)/subject")!, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: ["Authorization": "Bearer " + api_Key,"Content-Language" : lang ]).responseJSON { (response) in
            if response.result.isSuccess {
              print(response)
                let json =  JSON(response.data!)
                if json["status"]["succeed"].intValue == 1 {
                    SVProgressHUD.dismiss()
                    if let packageJSON = json["result"]["packages"].array {
                        //print(response)
                        self.createPackagesArray(json: packageJSON)
                    }
                   self.createSubjectsArray(json: json["result"]["subjects"].array!)
                    
                }else{
                    // trying to retrive from realm database
                    print("trying to retrive from realm database")
                    if self.realmSubjects() != nil || self.realmPackages() != nil {
                        SVProgressHUD.dismiss()
                        if let myPackage = self.realmPackages() {
                            self.packages = myPackage
                            self.instantiatePackagesCustomView()
                        }
                        if let mySubjects = self.realmSubjects() {
                            self.subjects = mySubjects
                            self.instantiateSubjectCustomViews()
                        }
                        
                        
                    }else{
                        SVProgressHUD.dismiss()
                        print("failed")
                        self.noConnectionImage.isHidden = false
                    }
                }
            }else{
                // trying to retrive from realm database
                print("trying to retrive from realm database")
                if self.realmSubjects() != nil || self.realmPackages() != nil {
                    SVProgressHUD.dismiss()
                    if let myPackage = self.realmPackages() {
                        self.packages = myPackage
                        self.instantiatePackagesCustomView()
                    }
                    if let mySubjects = self.realmSubjects() {
                        self.subjects = mySubjects
                        self.instantiateSubjectCustomViews()
                    }
                   
                    
                }else{
                    SVProgressHUD.dismiss()
                    print("failed")
                    self.noConnectionImage.isHidden = false
                }
            }
        }
        
    }

    func createSubjectsArray(json : [JSON]){
        
        
        if json.count > 0 {
        for index in 0...json.count - 1 {
            
            let subject = Subject()
            subject.id = json[index]["id"].intValue
            subject.imageUrl = json[index]["image_url"].stringValue
            subject.termFullName = json[index]["term_full_name"].stringValue
            subject.avgRate = json[index]["avg_rate"].intValue
            subject.price = json[index]["price"].intValue
            subject.download = json[index]["download"].intValue
            subject.title = json [index]["title"].stringValue
            subject._class = _classTitle!
            subject.classID = classID!
            subjects.append(subject)
            subject.store()
        }
        }
        
        instantiateSubjectCustomViews()
        
    }
    func instantiateSubjectCustomViews(){
        
        for subject in subjects {
         let subjectController = SubjectCustomController(frame: CGRect.zero)
            subjectController._classTitle = _classTitle!
            
        subjectController.subject = subject
            subjectController.addToCartButton.addTarget(self, action: #selector(self.addSubjectToCart(sender:)), for: .touchUpInside)
            subjectController.updateView()
            
            subjectsStackView.addArrangedSubview(subjectController.view)
            
            let constraint = NSLayoutConstraint(item: subjectController.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 200)
            let constraint2 = NSLayoutConstraint(item: subjectController.view, attribute: .width, relatedBy: .equal, toItem: subjectsScrollView, attribute: .width, multiplier: 1, constant: -40 )
            NSLayoutConstraint.activate([constraint,constraint2])
                        let gesture = UITapGestureRecognizer(target: self, action: #selector(subjectViewTapped(_:)))
                        subjectController.view.addGestureRecognizer(gesture)
            
        }
    }

    func createPackagesArray (json : [JSON]){
       // print(json.count)
        for index in 0...json.count-1{
            
            
            let package = Package()
            package.id = json[index]["id"].intValue
            package.imageUrl = json[index]["image_url"].stringValue
            package.name = json[index]["name"].stringValue
            package.originalPrice = json[index]["original_price"].intValue
            package.price = json[index]["price"].intValue
            package.term = json[index]["term_full_name"].stringValue
            package._class = _classTitle!
            package.classID = classID!
            let jsonSubjects = json[index]["subjects"].array!
            if jsonSubjects.count > 0 {
            for index2 in 0...(jsonSubjects.count)-1{
                
                let subject = Subject()
                subject.id = jsonSubjects[index2]["id"].intValue
                subject.download = jsonSubjects[index2]["download"].intValue
                subject.title = jsonSubjects[index2]["title"].stringValue
                subject.price = jsonSubjects[index2]["price"].intValue
                package.subjects.append(subject)
            }
            }
            package.store()
            packages.append(package)
        }
        instantiatePackagesCustomView()
        
    }
    func instantiatePackagesCustomView (){
     
        for package in packages {
            
            let packageController = PackageCustomController(frame: CGRect.zero)
            packageController._classTitle = _classTitle!
            packageController.package = package
            packageController.addToCartButton.addTarget(self, action: #selector(self.addPackageToCart(sender:)), for: .touchUpInside)
            packageController.updateView()
            
            subjectsStackView.addArrangedSubview(packageController.view)
            
            let constraint = NSLayoutConstraint(item: packageController.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 200)
            let constraint2 = NSLayoutConstraint(item: packageController.view, attribute: .width, relatedBy: .equal, toItem: subjectsScrollView, attribute: .width, multiplier: 1, constant: -40 )
            NSLayoutConstraint.activate([constraint,constraint2])
            
            
        }
    }
    
    
    @objc func subjectViewTapped(_ sender : UITapGestureRecognizer){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subjectDetailsViewController = storyboard.instantiateViewController(withIdentifier: "SubjectDetails") as! SubjectDetailsViewController
        let subjectViewController = sender.view?.resonsibleController as! SubjectCustomController
        
        subjectDetailsViewController.subjectId = subjectViewController.subject!.id
        subjectDetailsViewController.isPurchased = subjectViewController.subject!.download == 1
        present(subjectDetailsViewController, animated: false, completion: nil)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        
        let window = (UIApplication.shared.delegate as! AppDelegate).window
        
        UIView.transition(with: window!, duration: 0.5, options: .transitionFlipFromRight, animations: nil, completion: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func addPackageToCart (sender : UIButton){
        
        let packageController = sender.resonsibleController as! PackageCustomController
        packageController.addTocart()
        sendPayment(id: (packageController.package?.id)!, type: "package")
    }
    @objc func addSubjectToCart (sender : UIButton){
        
        let subjectController = sender.resonsibleController as! SubjectCustomController
        subjectController.addTocart()
        sendPayment(id: subjectController.subject!.id, type: "subject")
        
        
    }
    
    
     func sendPayment(id : Int , type :String) {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        var packageIDs = [String]()
        var subjectIDs = [String]()
        if type == "package"{
            packageIDs.append("\(id)")
        }else if type == "subject" {
            subjectIDs.append("\(id)")
        }

        let parameters: [String: Any] = [
            "Data":
                [
                    "action": "Payment",
                    "lang": lang
            ],
            "Request":
                [
                    "api_key":api_Key,
                    "user_id":userID,
                    "platform":"ios",
                    "subject_ids":subjectIDs,
                    "package_ids":packageIDs,
                    "payment_type":"knet"
                    
                    
            ]
        ]
        print(parameters)
        Alamofire.request(URL(string: "https://kashcool.com.kw/api")!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            print(response)
            if response.result.isSuccess{
                let json = JSON(response.data!)
            //print(json)
            self.showErrorMessage(title: "", message: json["Status"]["message"].stringValue)
            SVProgressHUD.dismiss()
            }else{
                
                let title = lang == "ar" ? "خطا" : "Error"
                let message = lang == "ar" ? "تاكد من الاتصال" : "Please check your connction"
                self.showErrorMessage(title: title, message: message)
            }
        }
        
        
    }
    
    
    
    func realmSubjects () -> [Subject]? {
        let results = try! Realm().objects(Subject.self)
        let array = Array(results).filter({$0.language == lang && $0.classID == classID})
        return array.count > 0 ? array : nil
    }
    func realmPackages () -> [Package]? {
        let results = try! Realm().objects(Package.self)
        let array = Array(results).filter({$0.language == lang && $0.classID == classID})
        return array.count > 0 ? array : nil
    }
    
     func showErrorMessage(title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = lang == "ar" ? "حسنا" : "Ok"
        let action = UIAlertAction(title: ok, style: .cancel, handler: nil)
        alert.addAction(action)
         self.present(alert, animated: true)
    }
    
}
