//
//  FeedBackViewController.swift
//  Kashcool
//
//  Created by aya on 10/9/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import UIKit
import RealmSwift
class FeedBackViewController: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var commentsStackView: UIStackView!
    var feedBacks = List<FeedBack>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let scale = lang == "en" ? -1 : 1
        backBtn.transform = CGAffineTransform(scaleX: CGFloat(scale), y: 1)
        for feedBack in feedBacks {
            
            let feedBackController = FeedBackController()
            feedBackController.feedBack = feedBack
            feedBackController.updateView()
            commentsStackView.addArrangedSubview(feedBackController.view)
            let constraint = NSLayoutConstraint(item: feedBackController.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 146)
            let constraint2 = NSLayoutConstraint(item: feedBackController.view, attribute: .width, relatedBy: .equal, toItem: commentsStackView, attribute: .width, multiplier: 1, constant: -20 )
            NSLayoutConstraint.activate([constraint,constraint2])
        }
    }

    @IBAction func backButtonPressed(_ sender: UIButton) {
        let window = (UIApplication.shared.delegate as! AppDelegate).window
        
        UIView.transition(with: window!, duration: 0.5, options: .transitionFlipFromRight, animations: nil, completion: nil)
        
        self.dismiss(animated: true, completion: nil)
    }

}
