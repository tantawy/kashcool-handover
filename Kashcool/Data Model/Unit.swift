//
//  Unit.swift
//  Kashcool
//
//  Created by aya on 10/3/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class Unit : Object{
    @objc dynamic var id = 0{
        didSet{
            uniqueID = "\(id).\(lang)"
            language = lang
        }
    }
    @objc dynamic var uniqueID = ""
    @objc dynamic var language = ""
    @objc dynamic var title = ""
    let lessons = List<Lesson>()
    
    
    override static func primaryKey () -> String{
        return "uniqueID"
    }
}
