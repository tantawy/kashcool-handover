//
//  Subject.swift
//  Kashcool
//
//  Created by aya on 9/27/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class Subject : Object {
    
    
    @objc dynamic var id = 0 {
        didSet{
            uniqueID = "\(id).\(lang)"
            language = lang
        }
    }
    @objc dynamic var language = ""
    @objc dynamic var classID = 0 {
        didSet {
            uniqueID.append(".\(self.classID)")
        }
    }
    @objc dynamic var uniqueID = ""
    @objc dynamic var title = ""
    @objc dynamic var price = 0
    @objc dynamic var termFullName = ""
    @objc dynamic var imageUrl = ""
    @objc dynamic var avgRate = 5
    @objc dynamic var download = 0
    @objc dynamic var _class = ""
    
    override static func primaryKey () -> String {
        return "uniqueID"
    }
    
    
    func store() {
        
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(self , update: true)
            }
        }catch{
            print(error)
        }
    }
}
