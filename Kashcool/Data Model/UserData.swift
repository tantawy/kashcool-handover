//
//  UserData.swift
//  Kashcool
//
//  Created by aya on 11/5/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class UserData: Object {
    
    @objc dynamic var lastLoggedInUser = "last"
    @objc dynamic var fullName = ""
    @objc dynamic var email = ""
    @objc dynamic var mobile = ""
    @objc dynamic var api_Key = ""
    @objc dynamic var userFolderName = ""
    @objc dynamic var classID = 0
    @objc dynamic var userID = 0
    @objc dynamic var userType = ""
    @objc dynamic var avatarUrl = ""
    
    
    override static func primaryKey () -> String {
        return "lastLoggedInUser"
    }
    static func defaultUser () -> UserData {
        
        let realm = try! Realm()
        if let myUser = realm.object(ofType: UserData.self, forPrimaryKey: "last"){
            print("FFFFFFF")
            print(myUser)
            return myUser

        }
        return UserData()
    }
    
    func store (){
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(self , update: true)
            }
        }catch{
            print(error)
        }
    }
}
