//
//  FeedBack.swift
//  Kashcool
//
//  Created by aya on 10/9/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class FeedBack : Object{
    
    @objc dynamic var id = 0{
        didSet{
            uniqueID = "\(id).\(lang)"
            language = lang
        }
    }
    @objc dynamic var uniqueID = ""
    @objc dynamic var language = ""
    @objc dynamic var comment = ""
    @objc dynamic var userId = 0
    @objc dynamic var date = ""
    @objc dynamic var userName = ""
    @objc dynamic var imageUrl = ""
    @objc dynamic var userRate = 0
    
    override static func primaryKey () -> String{
        return "uniqueID"
    }
    
    
    
}
