//
//  Package.swift
//  Kashcool
//
//  Created by aya on 9/27/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class Package : Object{
    
    
    
    @objc dynamic var id = 0 {
        didSet {
            language = lang
            uniqueID = "\(id).\(lang)"
        }
    }
    
    @objc dynamic var language = ""
    @objc dynamic var classID = 0 {
        didSet {
            uniqueID.append(".\(self.classID)")
        }
    }
    @objc dynamic var uniqueID = ""
    @objc dynamic var name = ""
    @objc dynamic var price = 0
    @objc dynamic var originalPrice = 0
    @objc dynamic var imageUrl = ""
    @objc dynamic var term = ""
    @objc dynamic var _class = ""
    let subjects = List<Subject>()
    
    
    override static func primaryKey () -> String{
        return "uniqueID"
    }
    
    func store() {
        
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(self , update: true)
            }
        }catch{
            print(error)
        }
    }
    
    
    
    
}
