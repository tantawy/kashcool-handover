//
//  subject.swift
//  Kashcool
//
//  Created by aya on 9/25/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class MySubject : Object {
    
    
    @objc dynamic var id = 0 {
        didSet {
            uniqueID = "\(userID).\(id).\(lang)"
            _userID = userID
            language = lang
        }
    }
    @objc dynamic var _userID = 0
    @objc dynamic var language = ""
    @objc dynamic var uniqueID = ""
    @objc dynamic var title = ""
    @objc dynamic var termFullName = ""
    @objc dynamic var imageUrl = ""
    @objc dynamic var classTitleEn = ""
    @objc dynamic var classTitleAr = ""
    
    
    
    override static func primaryKey () -> String {
    
        return "uniqueID"
    }
    
    
    func store () {
        
        
        let realm = try! Realm()
        do{
            
            try realm.write {
                realm.add(self , update: true)
            }
            
        }catch{
            print(error)
        }
        
        
    }
}
