//
//  Invoice.swift
//  Kashcool
//
//  Created by aya on 10/14/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class Invoice : Object {
    @objc dynamic var id = 0{
        didSet{
            language = lang
            userId = userID
            uniqueID = "\(userId).\(lang).\(id)"
        }
    }
    @objc dynamic var userId = 0
    @objc dynamic var language = ""
    @objc dynamic var uniqueID = ""
    @objc dynamic var price = 0
    @objc dynamic var date = ""
    @objc dynamic var status = ""
    @objc dynamic var code = 0
    @objc dynamic var statusCode = 0
    
    
    override static func primaryKey () -> String{
        return "uniqueID"
    }
    
    func store() {
        
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(self , update: true)
            }
        }catch{
            print(error)
        }
    }
}
