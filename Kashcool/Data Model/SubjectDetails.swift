//
//  SubjectDetails.swift
//  Kashcool
//
//  Created by aya on 10/3/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class SubjectDetails : Object {
    @objc dynamic var id = 0{
        didSet{
            userId = userID
            language = lang
            uniqueID = "\(userID).\(lang).\(id)"
        }
    }
    @objc dynamic var uniqueID = ""
    @objc dynamic var userId = 0
    @objc dynamic var language = ""
    @objc dynamic var title = ""
    @objc dynamic var folderName = ""
    @objc dynamic var _description = ""
    @objc dynamic var price = 0
    @objc dynamic var imageUrl = ""
    @objc dynamic var themeUrl = ""
    @objc dynamic var className = ""
    @objc dynamic var userRate : Float = 0.0
    @objc dynamic var rate : Float = 0.0
    let units = List<Unit>()
    @objc dynamic var term = 0
    @objc dynamic var demoUrl = ""
    let feedBacks = List<FeedBack>()
    
    override static func primaryKey () -> String{
        return "uniqueID"
    }
    
    
    func store() {
        
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(self , update: true)
            }
        }catch{
            print(error)
        }
    }
}
