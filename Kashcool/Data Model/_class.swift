//
//  class.swift
//  Kashcool
//
//  Created by aya on 9/24/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class _class : Object{
    
    @objc dynamic var id = 0 {
        didSet{
            language = lang
            uniqeId = "\(lang).\(id)"
        }
    }
    @objc dynamic var uniqeId = ""
    @objc dynamic var title = ""
    @objc dynamic var imageUrl = ""
    @objc dynamic var language = ""
    override static func primaryKey() -> String? {
        return "uniqeId"
    }
    
    func store(){
        
         let realm =  try! Realm()
        do {
           
                try realm.write {
                    realm.add(self, update: true)
                }
            
        }catch{
            print(error)
        }
    }
  
}
