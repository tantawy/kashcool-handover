//
//  Lesson.swift
//  Kashcool
//
//  Created by aya on 10/3/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class Lesson : Object {
    @objc dynamic var id = 0{
        didSet{
            uniqueID = "\(id).\(lang)"
            language = lang
        }
    }
    @objc dynamic var uniqueID = ""
    @objc dynamic var language = ""
    @objc dynamic var title = ""
    @objc dynamic var imageUrl = ""
    @objc dynamic var unitID = ""
    @objc dynamic var url = ""
    @objc dynamic var lessonName = ""
    
    override static func primaryKey () -> String{
        return "uniqueID"
    }
    
}
