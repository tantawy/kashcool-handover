//
//  DemoSubject.swift
//  Kashcool
//
//  Created by aya on 9/26/18.
//  Copyright © 2018 roqay. All rights reserved.
//

import Foundation
import RealmSwift
class DemoSubject : Object{
    
    
    @objc dynamic var id = 0 {
        didSet{
            language = lang
            uniqueID = "\(id).\(lang)"
        }
    }
    
    @objc dynamic var _classID = 0{
        didSet {
            uniqueID.append(".\(_classID)")
        }
    }
    @objc dynamic var language = ""
    @objc dynamic var uniqueID = ""
    @objc dynamic var demoTitle = ""
    @objc dynamic var subjectTitle = ""
    @objc dynamic var imageUrl = ""
    @objc dynamic var demoUrl = ""
    @objc dynamic var term = ""
    
    
    
    override static func primaryKey () -> String {
        return "uniqueID"
    }
    
    func store() {
        
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(self , update: true)
            }
        }catch{
            print(error)
        }
    }
}
